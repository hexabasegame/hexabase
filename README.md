TODO:

06/05:

 - BUG : if a lumberjack shack is right next to a lumbermill, the resource is not moved to the lumbermill, but removed from the jumberjack shack. No worker appear. Investigate why.

 - For the entrance, we need to check if there is any "open" hexa around the entrance. If not, an other direction should be chosen and entrance recalculated.
 

14/04:

 - Most important thing to do is to enhance the buildings "entrance". It can't be just one place, it must be anywhere around the building.

03/04:

 - Buildings should be able to get their resources from near production center without need of a warehouse

 - The warehouse needs a smarter way in it's workers management, a single worker should be enough for 6 or 7 buildings

 - Right click should open a small construction menu

 - DONE Massively reduce the construction icons. The buildings icon should have the same icon as the resource they produce for coherence sake.

 - DONE Buildings construction should display the grid and colorize the building that produce its needed resource and are in range of getting those itself

 - DONE Explore a way to generate ground based on hexCell type.