﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TerrainData = UnityEngine.TerrainData;

public class StartMenu : MonoBehaviour {

	public GameObject panelLoadFile;
	public GameObject panelLoadContent;
	public GameObject panelMenuStart;
	public GameObject loadingScreen;

	public GameObject ButtonLoadFile;


	void Start()
	{
		panelLoadFile.SetActive (false);
		loadingScreen.SetActive(false);
	}

	void Update()
	{
		if (panelLoadFile.activeSelf && Input.GetKeyDown (KeyCode.Escape))
			panelLoadFile.SetActive (false);
	}

	
	public void ButtonStartGame() 
	{
		DeleteAllChildren (panelLoadContent.transform);
		loadingScreen.SetActive(true);
		panelMenuStart.SetActive(false);
		SceneManager.LoadScene ("Scene Game");
	}
    public void ButtonQuit(){
        AppHelper.Quit();
    }

	public void ButtonLoadPanel()
	{
		panelMenuStart.SetActive(false);
		panelLoadFile.SetActive (true);
		DeleteAllChildren (panelLoadContent.transform);
		foreach (string fileName in SaveMaster.instance.saveFiles)
			InstanciateButtonOnContent (panelLoadContent, ButtonLoadFile, fileName);
	}

	public void ButtonBack()
	{
		panelLoadFile.SetActive (false);
		panelMenuStart.SetActive(true);
	}

	void InstanciateButtonOnContent(GameObject content, GameObject buttonPrefab, string fileName)
	{
		GameObject go = Instantiate (buttonPrefab) as GameObject;
		go.transform.SetParent (content.transform, false);
		go.GetComponent<SaveLoadButton> ().FileName = fileName;
	}

	void DeleteAllChildren (Transform parent)
	{
		foreach (Transform child in parent)
			Destroy(child.gameObject);
	}
}

 public static class AppHelper
 {
     #if UNITY_WEBPLAYER
     public static string webplayerQuitURL = "http://google.com";
     #endif
     public static void Quit()
     {
         #if UNITY_EDITOR
//         EditorApplication.isPlaying = false;
         #elif UNITY_WEBPLAYER
         Application.OpenURL(webplayerQuitURL);
         #else
         Application.Quit();
         #endif
     }
 }