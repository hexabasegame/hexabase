﻿using UnityEngine;
using UnityEngine.UI;

public class PanelPause : MonoBehaviour {

	public static PanelPause instance = null;

	// panel ref
	public GameObject panelMenu;
	public GameObject panelSave;
	public GameObject panelSaveContent;
	public GameObject panelLoad;
	public GameObject panelLoadContent;
	public InputField inputFieldSaveFile;
	public Text infoText;

	// prefabs
	public GameObject ButtonSaveFile;
	public GameObject ButtonLoadFile;

	void Start()
	{
		if (instance == null)
			instance = this;
	}

	void OnEnable()
	{
		InfoTextDisplay ("", Color.white);
	}

	public void Close()
	{
		if (panelSave.activeSelf)
			panelSave.SetActive (false);
		else if (panelLoad.activeSelf)
			panelLoad.SetActive (false);
		else {
			panelMenu.SetActive (false);
			gameObject.SetActive (false);
			InputMaster.instance.GameState = GAMESTATE.DEFAULT;
		}
	}

	public void Open()
	{
		panelMenu.SetActive (true);
		ButtonBack ();
	}

	public void ButtonBack()
	{
		panelSave.SetActive (false);
		panelLoad.SetActive (false);
	}

	public void ButtonSavePanel(){
		panelSave.SetActive (true);
		DeleteAllChildren (panelSaveContent.transform);
		foreach (string fileName in SaveMaster.instance.saveFiles)
			InstanciateButtonOnContent (panelSaveContent, ButtonSaveFile, fileName);
	}

	public void ButtonLoadPanel(){
		panelLoad.SetActive (true);
		DeleteAllChildren (panelLoadContent.transform);
		foreach (string fileName in SaveMaster.instance.saveFiles)
			InstanciateButtonOnContent (panelLoadContent, ButtonLoadFile, fileName);
	}

	public void ButtonSave()
	{
		SaveFile (inputFieldSaveFile.text);
	}

	public void TextInputNewSave(string newSave)
	{
		SaveFile (newSave);
	}

	void InstanciateButtonOnContent(GameObject content, GameObject buttonPrefab, string fileName)
	{
		GameObject go = Instantiate (buttonPrefab) as GameObject;
		go.transform.SetParent (content.transform, false);
		go.GetComponent<SaveLoadButton> ().FileName = fileName;
	}

	void DeleteAllChildren (Transform parent)
	{
		foreach (Transform child in parent)
			Destroy(child.gameObject);
	}

	public void ResetInputField(string str = "")
	{
		inputFieldSaveFile.text = str;
	}

	public void OnTyping()
	{
		InfoTextDisplay ("", Color.white);
	}

	public void InfoTextDisplay(string str, Color color)
	{
		if (!infoText)
			return;
		infoText.text = str;
		infoText.color = color;
	}

	void SaveFile(string newSave)
	{
		if (string.IsNullOrEmpty (newSave)) {
			InfoTextDisplay ("- Name can not be Empty -", Color.red);
			return ;
		}

		if (SaveMaster.instance.saveFiles.Contains (newSave)) {
			var idx = SaveMaster.instance.saveFiles.FindIndex(x => x == newSave);
			string item = SaveMaster.instance.saveFiles[idx];
			SaveMaster.instance.saveFiles.RemoveAt(idx);
		}
		SaveMaster.instance.saveFiles.Add (newSave);

		ButtonSavePanel ();
		SaveLoadButton save = panelSaveContent.transform.GetChild (0).GetComponent<SaveLoadButton> ();
		save.FileName = newSave;
		SaveMaster.instance.SaveGame(newSave);
		ResetInputField ();
		InfoTextDisplay ("- Game Saved -", Color.green);
		Debug.Log ("Saving " + newSave);


	}
}
