﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SaveLoadButton : MonoBehaviour {

	string fileName;

	public void ButtonSave()
	{
		if (PanelPause.instance.gameObject.activeSelf) {
			PanelPause.instance.ResetInputField (fileName);
			PanelPause.instance.InfoTextDisplay ("", Color.white);
		}
	}

	public void ButtonLoad() 
	{
	    SaveMaster.instance.FilePath = fileName;
	    SceneManager.LoadScene("Scene Game");
		Debug.Log ("Loading " + fileName);
	}

	public void ButtonDelete() 
	{
		SaveMaster.instance.DeleteGame (fileName);
		Destroy (gameObject);
	}

	public string FileName {
		get {
			return fileName;
		}
		set {
			fileName = value;
			transform.GetChild (0).GetComponent<Text> ().text = fileName;
		}
	}
}
