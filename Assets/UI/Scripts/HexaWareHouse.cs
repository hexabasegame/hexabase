﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexaWareHouse : HexaBuilding {

	public int scanRange = 50;

	private Dictionary<HexaFactory, int> inRangeFactories;
	private List<HexaFactory> sortedFactories;
	private int _interval;
	
	void Awake() {
		_interval = WorkersCount;
	}


	void Start() {
		base._type = BuildingType.Warehouse;
	}

	void Update() { }

	IEnumerator SendWorkers() {
		List<HexaFactory> dependantFactories = new List<HexaFactory>(sortedFactories);
		while (true) {
			foreach (HexaWorker worker in Workers) {
				if (dependantFactories.Count == 0)
					dependantFactories = new List<HexaFactory>(sortedFactories);
				if (worker.Behavior.currentState == MovingState.WAITING_FOR_PATH && dependantFactories.Count > 0) {
					HexaFactory targetFactory = dependantFactories[0];
					dependantFactories.RemoveAt(0);
					StartCoroutine("SendWorkerToFactory",
						new Dictionary<HexaWorker, HexaFactory> {{worker, targetFactory}}
					);
					yield return new WaitForSeconds(1);
				}
			}
			yield return new WaitForSeconds(_interval);
		}
	}

	IEnumerator SendWorkerToFactory(Dictionary<HexaWorker, HexaFactory> couple) {
		foreach (KeyValuePair<HexaWorker, HexaFactory> pair in couple) {
			HexaWorker worker = pair.Key;
			HexaFactory factory = pair.Value;

			List<HexCell> path = HexaPath.GetPathFromXtoY(Entrace, pair.Value.Entrace);
			if (path == null) {
				yield return new WaitForSeconds(1);
			}
			worker.Behavior.SetNewPath (path);
			LoadWorkerWithNeededResource (factory, worker);
			while (worker.Behavior.currentState != MovingState.WAITING_FOR_PATH) {
				yield return new WaitForSeconds(1);
			}
		}
	}

	void LoadWorkerWithNeededResource(HexaFactory factory, HexaWorker worker){
		worker.Destination = factory;
		worker.Carry = factory.MostNeededResource();
		if (worker.Carry != null) {
			worker.Carry.amount = Math.Min(worker.Carry.amount,
				Math.Min(worker.MaxCapacity, ResourceMaster.instance.GetGlobalResource(worker.Carry).stockAmount));
			ResourceMaster.instance.RemoveGlobalResourceAmount(worker.Carry);
		}
	}

	/*
	 * Two way of doing this :
	 * First, as is now, just do a "weighting" of every case for a specified distance and store
	 * every building found on the way.
	 * Second way, just check the list of every building and calculate their distances. If building-distance < threshold
	 * store the building.
	*/
	public void ScanRangeWithCellsReading() {
		List<HexCell> toWeight = new List<HexCell> { };
		List<HexCell> scanedNeighbours = new List<HexCell>();
		Dictionary<HexCell, int> weightTable = new Dictionary<HexCell, int>();
		weightTable[GridPosition] = 0;
		inRangeFactories = new Dictionary<HexaFactory, int>();
		scanedNeighbours.Add(GridPosition);
		toWeight.Add(GridPosition);

		int distance = 0;
		while (distance < scanRange && toWeight.Count > 0) {
			HexCell current = toWeight[0];
			toWeight.RemoveAt(0);
			foreach (HexCell nextNeighboor in current.Neighboors) {
				if (nextNeighboor.Building != null &&
					nextNeighboor.Building.GetType() == typeof(HexaFactory)) {
					inRangeFactories[(HexaFactory) nextNeighboor.Building] = distance;
				}
				if (!scanedNeighbours.Contains(nextNeighboor)) {
					distance = weightTable[current] + 1;
					weightTable[nextNeighboor] = distance;
					scanedNeighbours.Add(nextNeighboor);
					toWeight.Add(nextNeighboor);
				}
			}
		}
		BuildSortedByRangeListOfFactories();
	}

	public void ScanRangeWithDistance() {
		inRangeFactories = new Dictionary<HexaFactory, int>();
		foreach (HexaFactory factory in TimeMaster.instance.Factories) {
			int range = HexGrid.GetDistanceBetweenTwoCells(factory.GridPosition, this._gridPosition);
			if (range <= scanRange) {
				inRangeFactories.Add(factory, range);
			}
		}
		BuildSortedByRangeListOfFactories();
	}

	private void BuildSortedByRangeListOfFactories() {
		sortedFactories = new List<HexaFactory>();
		for (int i = 0; i < scanRange; i++) {
			foreach (KeyValuePair<HexaFactory, int> factory in inRangeFactories) {
				if (factory.Value == i) {
					sortedFactories.Add(factory.Key);
				}
			}
		}
	}

	public override void OnConstruction(HexCell cell) {
		base.OnConstruction(cell);
		_type = BuildingType.Warehouse;
		TimeMaster.instance.AddBuilding(this);
	}

	public override void OnDestroy() {
		foreach (HexaWorker worker in Workers) {
//			workers.Remove (worker);
			if (worker == null)
				continue;

			if (worker.Carry != null && worker.Carry.amount != 0) {
				ResourceMaster.instance.AddGlobalResourceAmount(worker.Carry);
			}
			Destroy (worker.gameObject);
		}
		TimeMaster.instance.RemoveBuilding(this);
	}

	public bool CanSendWorkerWithResource(Resource resource, HexCell destination) {
		foreach (Resource res in ResourceMaster.instance.GlobalStocks.Values) {
			if (res.type.name != resource.type.name) { continue; }
			if (res.stockAmount < 1) { break; }
			HexaWorker availableWorker = GetAvailableWorker();
			if (availableWorker != null) {
				// Send worker to 'destination'
				List<HexCell> path = HexaPath.GetPathFromBuildingToBuilding(this._gridPosition, destination);
				if (path == null) { return false; }
				availableWorker.Behavior.SetNewPath(path);

				// LoadWorkerWithNeededResource
				availableWorker.Carry = resource;
				availableWorker.Carry.amount = 1;

				availableWorker.Origin = this;
				availableWorker.Destination = (HexaBuilding) destination.Building;

				ResourceMaster.instance.RemoveGlobalResourceAmount(availableWorker.Carry);
				return true;
			}
		}
		return false;
	}

	public bool CanSendWorkerForResource(Resource resource, HexCell destination) {
		HexaWorker availableWorker = GetAvailableWorker();
		if (availableWorker != null) {
			List<HexCell> path = HexaPath.GetPathFromBuildingToBuilding(this._gridPosition, destination);
			if (path == null || path.Count == 0) { return false; }
			availableWorker.Behavior.SetNewPath(path);

			// LoadWorkerWithNeededResource
			availableWorker.Carry = null;

			availableWorker.Origin = this;
			availableWorker.Destination = (HexaBuilding) destination.Building;
			return true;
		}
		return false;
	}


	//    public override void DestroyBuilding(HexaRenderable building, HexCell cell) {
	//        base.Dest
	//    }


	//    private void BuildSortedByInputNeededListOfFactories() {
	//        sortedFactories = new List<HexaFactory>();
	//        for (int i = 0; i < _range; i++) {
	//            foreach (KeyValuePair<HexaFactory, int> factory in inRangeFactories) {
	//                if (factory.Key. == i) {
	//                    sortedFactories.Add(factory.Key);
	//                }
	//            }
	//        }
	//    }
}
