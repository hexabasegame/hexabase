﻿using UnityEngine;

public class WorkerActivityBox : MonoBehaviour {

	public HexaWorker worker;

	public void FocuskButton()
	{
		UIMaster.instance.panelDescription.PutFocusOnWorker (worker);
	}

	public void ForceReturnButton()
	{
		worker.Behavior.ForceReturn ();
	}

	public void PauseButton()
	{
		worker.Behavior.Pause ();
	}
}
