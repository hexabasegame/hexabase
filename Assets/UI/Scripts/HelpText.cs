﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HelpText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler{

	public string helpText = "no text";

	Text panelText = null;

	[HideInInspector] public HexaBuilding building = null; // set at start() for a assigned building or by DescriptionPanel if upgrade exists
	[HideInInspector] public HexaTier tier = null; // set by UImaster if helpText must display tiers infos
	[HideInInspector] public bool isDisplaying = false;

	void Start ()
	{
		panelText = UIMaster.instance.panelHelpText.transform.GetChild(0).GetComponent<Text>();
		ConstructionButton button = gameObject.GetComponent<ConstructionButton>();
		if (button)
			building = button.building;
	}

	public void DisplayText(string text){
		panelText = UIMaster.instance.panelHelpText.transform.GetChild(0).GetComponent<Text>();
		helpText = text;
		panelText.text = text;
	}

	public void OnPointerEnter (PointerEventData eventData)
	{
		isDisplaying = true;
		UIMaster.instance.panelHelpText.SetActive (true);
		DisplayText(helpText);
		if (building != null)
			UIMaster.instance.DisplayBuildingHelperInfo(building);
		else if (tier != null)
			UIMaster.instance.DisplayTierRequirement(tier);
	}

	public void OnPointerExit (PointerEventData eventData)
	{
		isDisplaying = false;
		UIMaster.instance.panelHelpText.SetActive (false);
		panelText.text = "";
		if (building != null)
			UIMaster.instance.DestroyBuilingHelperInfo();
		else if (tier != null)
			UIMaster.instance.DestroyBuilingHelperInfo();
	}
}
