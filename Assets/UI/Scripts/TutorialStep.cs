﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class TutorialPage {
	public string title;
	public Sprite sprite = null;
	public string explanation;
}

public class TutorialStep : MonoBehaviour {

	public string stepName;
	public List<TutorialPage> pages;
	
	int currentPageIndex = 0;


	public int CurrentPageIndex {
		get {
			return currentPageIndex;
		}
	}

	public TutorialPage GetCurrentPage() {
		return pages[currentPageIndex];	
	}

	public TutorialPage NextPage ()
	{
		if (currentPageIndex + 1 < pages.Count) {
			currentPageIndex ++;
			return GetCurrentPage();
		}
		return null;
	}

	public TutorialPage PrevPage() 
	{
		if (currentPageIndex - 1 >= 0) {
			currentPageIndex --;
			return GetCurrentPage();
		}
		return null;
	}

	public bool IsLastPage() 
	{
		return currentPageIndex == pages.Count - 1;
			
	}

	public bool IsFirstPage() 
	{
		return currentPageIndex == 0;
	}

	public void ResetStep()
	{
		currentPageIndex = 0;
	}
}
