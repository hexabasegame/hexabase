﻿using System.Collections.Generic;
using UnityEngine;

public class Gridhelper : MonoBehaviour {

	public GameObject hexagonePrefab;

	float height = 0.20f;
	List<GameObject> tiles = new List<GameObject>();
	HexCell currentTile = null;
	bool updateColors = false;

	void Update()
	{
		if (!gameObject.activeSelf)
			return;
		if (updateColors && MouseHasEnterNewTile ()) {
			RefreshTilesColor (BuildMaster.instance.SelectedBuilding);
		}
	}

	public void GenerateCircularGrid(Vector3 origin, int ringsCount)
	{
		ResetGrid();
		InstanciateHexagon (transform.position);
		for (int ring = 1; ring <= ringsCount; ring++) 
		{
			float ringInnerRadius = HexMetrics.innerRadius * ring * 2;
			for (float current_angle = 0; current_angle < 360f; current_angle += 60) 
			{
				float rad = (current_angle) * Mathf.Deg2Rad;
				float x = origin.x + (Mathf.Cos (rad) * ringInnerRadius);
				float z = origin.z + (Mathf.Sin (rad) * ringInnerRadius);
				Vector3 position = new Vector3 (x, height, z);
				InstanciateHexagon (position);
				TraceHexagonLineFromPosition(position, current_angle + 120, ring -1);
			}
		}
	}

	void TraceHexagonLineFromPosition(Vector3 startPosition, float deg, int count)
	{
		for (int i = 0; i < count; i++) 
		{
			float offset = HexMetrics.innerRadius * 2 * (i+1);
			float rad = deg * Mathf.Deg2Rad;
			float x = startPosition.x + (Mathf.Cos (rad) * offset);
			float z = startPosition.z + (Mathf.Sin(rad) * offset);
			InstanciateHexagon (new Vector3(x, height, z));
		}
	}

	void InstanciateHexagon(Vector3 position)
	{
		GameObject go = Instantiate (hexagonePrefab, position, hexagonePrefab.transform.localRotation);
		go.transform.SetParent (transform);
		SpriteRenderer sprite = go.GetComponent<SpriteRenderer>();
		sprite.color = UIMaster.instance.colorPalet[(int)COLOR_PALET.GREEN];
		tiles.Add (go);
	}

	bool MouseHasEnterNewTile()
	{
		HexCell raycastTile = RaycastMaster.instance.MouseOverCell;

		if (raycastTile == null || raycastTile == currentTile)
			return false;
		currentTile = raycastTile;
		return true;
	}

	void RefreshTilesColor(HexaBuilding building)
	{
		foreach(GameObject tile in tiles){
			SpriteRenderer sprite = tile.GetComponent<SpriteRenderer> ();

			HexCoordinates coordinates = HexCoordinates.FromPosition(tile.transform.position);
			int index = coordinates.X + coordinates.Z * HexMetrics.width + coordinates.Z / 2;

			if (index < 0 || index >= HexGrid.GetCells ().Length) {
				tile.SetActive (false);
				continue;
			}
			tile.SetActive (true);
			HexCell cell = HexGrid.GetCells()[index];
			Color color;

			if (building.canBePlacedOn.Contains(cell.cellType) && (building.mustBePlacedOn.Count == 0 || building.mustBePlacedOn.Count > 0 && building.mustBePlacedOn.Contains(cell.cellType)))
				color = UIMaster.instance.colorPalet[(int)COLOR_PALET.GREEN];
			else
				color = UIMaster.instance.colorPalet[(int)COLOR_PALET.RED];
			sprite.color = new Color (color.r, color.g, color.b, 0.8f);
		}
	}

	void OnEnable()
	{
		HexaBuilding building = BuildMaster.instance.SelectedBuilding;
		if (building != null)
			RefreshTilesColor (building);
	}
	
	void ResetGrid ()
	{
		for (int i=0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			tiles.Remove(child.gameObject);
			Destroy (child.gameObject);
		}
	}

	public void GenerateScanRangeBuildingGrid(HexaBuilding building, Vector3 position, bool updateColors=false) 
	{
		this.updateColors = updateColors;
		int buildingRange = 0;
		if (building is HexaFactory) {
			buildingRange = ((HexaFactory)building).scanRange;
		} else if (building is HexaAoEBuilding) {
			buildingRange = ((HexaAoEBuilding)building).scanRange;
		} else if (building is HexaWareHouse){
			buildingRange = ((HexaWareHouse)building).scanRange;
		}
		GenerateCircularGrid (position, buildingRange);
	}
}
