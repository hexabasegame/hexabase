﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DescriptionPanel : MonoBehaviour
{
    // UI elements ref
    public GameObject buildingDescription;
    public GameObject factoryDescription;
    public GameObject warehouseDescription;
    public GameObject houseDescription;
    public GameObject aoeBuildingDescription;

    // Building description ref
    public Text labelName;

    public GameObject buttonDestroyBuilding;

    // Destination description ref
    public GameObject buttonToggleProduction;
	public GameObject buttonUpgrage;
    public GameObject layoutOut;
    public GameObject layoutIn;
    public Slider factoryProductionSlider;
    public Text factoryProductionState;

    // warehouse description ref
    public GameObject warehouseScrollContent;
    public Text warehouseInfos;

    // house description ref
    public Text houseInfos;
    public GameObject houseNeedsContent;

    // prefabs
    public GameObject prefabBlockResourceH;
	public GameObject prefabBlockResourceGauge;
    public GameObject prefabBlockResource;
    public GameObject cursorSelection;
    public GameObject prefabWorkerActivity;
    public GameObject prefabWorkerFocus;

	// others
	public Gridhelper gridHelper;

    // public
    [HideInInspector] public bool isHidden = true;

    // private
    private Animator animator = null;

    private HelpText toggleProductionHelpText = null;

    private HexaWareHouse warehouse = null;
    private HexaFactory factory = null;
    private HexaBuilding building = null;
    private HexaRenderable renderable = null;
    private HexaHouse house = null;
    private HexCell cell = null;
	private HexaAoEBuilding aoeBuilding = null;

    // API //

    public void Display(HexCell cell)
    {
        this.cell = cell;
        if (!cell.HasBuilding)
            throw new UnityException("No building on Cell");
        isHidden = false;
		gridHelper.gameObject.SetActive(true);	
		gridHelper.GenerateScanRangeBuildingGrid((HexaBuilding)cell.Building, cell.position);
        SetCursorOnCell(cell);
        ResetFocusOnWorker();
        SetReferences();
        SetLabelName();
        ToggleRenderableDescription();
        SlideOpen();
    }


    public void Hide()
    {
        isHidden = true;
		gridHelper.gameObject.SetActive(false);	
        SlideClose();
        HideCursor();
        UnsetReferences();
        ResetFocusOnWorker();
    }

    public void RefreshFactoryResourceDisplay()
    {
        PopulateProductionInputOutput(factory);
    }

    public void RefreshWarehouseDisplay()
    {
        if (!warehouse)
            return;
        int workerCount = 0;
        foreach (HexaWorker worker in warehouse.Workers)
        {
            GameObject child = warehouseScrollContent.transform.GetChild(workerCount).gameObject;
            SetupWorkerActivityBlock(child, worker);
            workerCount++;
        }
    }

    public void PutFocusOnWorker(HexaWorker worker)
    {
        ResetFocusOnWorker();
        GameObject focus = Instantiate(prefabWorkerFocus, worker.transform.position, Quaternion.identity) as GameObject;
        focus.transform.SetParent(worker.transform);
    }

    public void RefreshFactoryInfoDisplay()
    {
        string status;
        if (factory.noWorkerAlert == false)
            status = "Status : " + TranslationFactoryState.translate[(int) factory.CurrentState] + Environment.NewLine;
        else
            status = "Status : No worker has come for a while !" + Environment.NewLine;
        string duration = "Time : " + factory.processTime.ToString() + " sec." + Environment.NewLine;
        string maintenance = "Maintenance : " + factory.maintenance.amount.ToString() + " / sec" + Environment.NewLine;
        factoryProductionState.text = status + duration + maintenance;
    }

    public void RefreshHouseDisplay()
    {
        SetHouseInfos(house);
        PopulateHouseNeeds(house);
    }

    // BUTTONS //

    public void ButtonClose()
    {
        Hide();
    }

    public void ButtonBuildingDestroy()
    {
        BuildMaster.instance.DestroyBuilding(building, cell);
        Hide();
    }

    public void ButtonToggleFactoryProduction()
    {
        if (factory.IsActive)
            DeactivateFactory(this.factory);
        else
            ActivateFactory(this.factory);
    }

	public void ButtonUpgrade ()
	{
		if (building.upgrade == null || !ResourceMaster.instance.HasEnoughGlobalResources (building.upgrade.cost))
			return;
		HexaBuilding upgrade = building.upgrade;
		Quaternion currentRotation = building.Model.transform.localRotation;
		Vector3 deltaRotation = currentRotation.eulerAngles - building.modelPrefab.transform.localRotation.eulerAngles;
		HexCell currentCell = building.GridPosition;
		BuildMaster.instance.SelectedBuilding = upgrade;
		ButtonBuildingDestroy();
		HexaBuilding newBuilding = BuildMaster.instance.ConstructBuildingOnCell(upgrade, currentCell, true);
		newBuilding.Model.transform.localRotation = Quaternion.Euler(newBuilding.Model.transform.localRotation.eulerAngles + deltaRotation);
		UIMaster.instance.SpawnSmokeOnCell(newBuilding.GridPosition, (float)BuildMaster.instance.SelectedBuilding.renderableSize);
	}

    // INTERNAL LOGIC //

    void Start()
    {
        cursorSelection = Instantiate(cursorSelection) as GameObject;
        cursorSelection.SetActive(false);
        animator = gameObject.GetComponent<Animator>();
        toggleProductionHelpText = buttonToggleProduction.GetComponent<HelpText>();
    }

    void Update ()
	{
		if (isHidden)
			return;
		if (building) {
			RefreshButtonUpgrade();
		}
        if (factory)
        {
            RefreshInputResourcesAvailabilityColor(factory);
            RefreshProductionSliderValue();
        }
        else if (house)
        {
            RefreshHouseDisplay();
        }
    }

    void SetReferences()
    {
        UnsetReferences();
        renderable = cell.Building;
        if (renderable is HexaBuilding)
            building = (HexaBuilding) renderable;
        if (renderable is HexaFactory)
            factory = (HexaFactory) renderable;
        if (renderable is HexaWareHouse)
            warehouse = (HexaWareHouse) renderable;
        if (renderable is HexaHouse)
            house = (HexaHouse) renderable;
		if (renderable is HexaAoEBuilding)
			aoeBuilding = (HexaAoEBuilding) renderable;
        renderable.isSelected = true;
    }

    void UnsetReferences()
    {
        if (renderable != null)
            renderable.isSelected = false;
        renderable = null;
        building = null;
        factory = null;
        warehouse = null;
        house = null;
		aoeBuilding = null;
    }

    void ToggleRenderableDescription ()
	{
		if (building) {
			buildingDescription.SetActive (true);
			DisplayUpgrage();
		}
		if (factory) {
			factoryDescription.SetActive (true);
			warehouseDescription.SetActive (false);
			houseDescription.SetActive (false);
			aoeBuildingDescription.SetActive(false);
			DisplayToggleProduction (factory);
			PopulateProductionInputOutput (factory);
			if (!HexaTutorialMaster.instance.skipTutorialMode)
				HexaTutorialMaster.instance.DisplayTutorial ("TutorialFactory");
		} else if (warehouse) {
			warehouseDescription.SetActive (true);
			factoryDescription.SetActive (false);
			houseDescription.SetActive (false);
			aoeBuildingDescription.SetActive(false);
			DisplayWarehouseInfos (warehouse);
			PopulateWarehouseWorkers (warehouse);
			if (!HexaTutorialMaster.instance.skipTutorialMode)
				HexaTutorialMaster.instance.DisplayTutorial ("TutorialWarehouse");
		} else if (house) {
			houseDescription.SetActive (true);
			warehouseDescription.SetActive (false);
			factoryDescription.SetActive (false);
			aoeBuildingDescription.SetActive(false);
			RefreshHouseDisplay ();
			if (!HexaTutorialMaster.instance.skipTutorialMode)
				HexaTutorialMaster.instance.DisplayTutorial ("TutorialHouse");
		} else if (aoeBuilding) {
			aoeBuildingDescription.SetActive(true);
			houseDescription.SetActive (false);
			warehouseDescription.SetActive (false);
			factoryDescription.SetActive (false);
		}
        else
        {
            UnsetReferences();
            buildingDescription.SetActive(true);
        }
    }

    void SlideOpen()
    {
        animator.SetBool("openPanel", true);
    }

    void SlideClose()
    {
        animator.SetBool("openPanel", false);
    }

    // 3D CURSOR //

    void SetCursorOnCell(HexCell cell)
    {
        cursorSelection.SetActive(true);
        cursorSelection.transform.position = cell.Building.Model.transform.position;
    }

    void HideCursor()
    {
        cursorSelection.SetActive(false);
    }


    // DESCRIPTION RENDERABLE //

    void SetLabelName()
    {
        labelName.text = building.name;
    }

    // DESCRIPTION BUILDING //

	void DisplayUpgrage ()
	{
		if (building.upgrade == null) {
			buttonUpgrage.SetActive (false);
			return;
		}

		// set button
		buttonUpgrage.SetActive (true);
		RefreshButtonUpgrade();

		// setup help text
		HelpText helper = buttonUpgrage.GetComponent<HelpText>();
		helper.helpText = "Upgrade to " + building.upgrade.name;
		helper.building = building.upgrade;
	}

	void RefreshButtonUpgrade ()
	{
		if (building.upgrade == null)
			return;

		Image buttonImage = buttonUpgrage.GetComponent<Image>();
		if (!ResourceMaster.instance.HasEnoughGlobalResources (building.upgrade.cost)) {
			buttonImage.color = UIMaster.instance.colorPalet [(int)COLOR_PALET.RED];
		} else
			buttonImage.color = UIMaster.instance.colorPalet [(int)COLOR_PALET.GREEN];
	}

    // DESCRIPTION FACTORY //

    void DisplayToggleProduction(HexaFactory factory)
    {
        if (factory.IsActive)
        {
            toggleProductionHelpText.DisplayText("Production : ON");
            ChangeButtonColor(buttonToggleProduction, COLOR_PALET.GREEN);
        }
        else
        {
            toggleProductionHelpText.DisplayText("Production : OFF");
            ChangeButtonColor(buttonToggleProduction, COLOR_PALET.RED);
        }
    }

    void ActivateFactory(HexaFactory factory)
    {
        factory.IsActive = true;
        DisplayToggleProduction(factory);
    }

    void DeactivateFactory(HexaFactory factory)
    {
        factory.IsActive = false;
        DisplayToggleProduction(factory);
    }

    void ChangeButtonColor(GameObject buttonObject, COLOR_PALET color)
    {
        Button button = buttonObject.GetComponent<Button>();
        Image image = buttonObject.GetComponent<Image>();
        image.color = UIMaster.instance.colorPalet[(int) color + 1];

        ColorBlock colors = button.colors;
        colors.normalColor = UIMaster.instance.colorPalet[(int) color - 1];
        colors.highlightedColor = UIMaster.instance.colorPalet[(int) color + 1];
        button.colors = colors;
    }

    void PopulateProductionInputOutput(HexaFactory factory)
    {
        DeleteAllChildren(layoutIn.transform);
        DeleteAllChildren(layoutOut.transform);

        foreach (Resource resource in factory.inputResource)
            AddResourceBlockToPanel(resource, layoutIn, prefabBlockResourceH);
        foreach (Resource resource in factory.outputResource)
            AddResourceBlockToPanel(resource, layoutOut, prefabBlockResourceH);
    }

    void AddResourceBlockToPanel(Resource resource, GameObject panel, GameObject prefab)
    {
        GameObject child = Instantiate(prefab);
        child.transform.SetParent(panel.transform, false);
        child.name = resource.type.name;
        child.GetComponent<HelpText>().helpText = resource.type.name;
        child.transform.GetChild(0).GetComponent<Image>().sprite = resource.type.icon;
        child.transform.GetChild(1).GetComponent<Text>().text = FormatResourceText(resource);
    }

    string FormatResourceText(Resource resource)
    {
        string amount = "x" + resource.amount.ToString();
        string maxStock = (resource.stockAmountMax == -1) ? "-" : resource.stockAmountMax.ToString();
        string stock = resource.stockAmount.ToString() + " / " + maxStock;
        return amount + " (" + stock + ")";
    }

    void DeleteAllChildren(Transform parent)
    {
        foreach (Transform child in parent)
            Destroy(child.gameObject);
    }

    void RefreshInputResourcesAvailabilityColor(HexaFactory factory)
    {
        foreach (Resource resource in factory.inputResource)
        {
            if (ResourceIsAvailable(resource))
                SetResourceColor(resource, COLOR_PALET.GREEN);
            else
                SetResourceColor(resource, COLOR_PALET.RED);
        }
    }

    void SetResourceColor(Resource resource, COLOR_PALET color)
    {
        foreach (Transform child in layoutIn.transform)
        {
            if (child.name == resource.type.name)
            {
                child.transform.GetChild(1).GetComponent<Text>().color = UIMaster.instance.colorPalet[(int) color - 1];
            }
        }
    }

    bool ResourceIsAvailable(Resource resource)
    {
        if (!factory.HasEnoughInputResourceAmount(resource, resource.amount))
            return false;
        return true;
    }

    void RefreshProductionSliderValue()
    {
        float factor = factory.TimeSpent / factory.processTime;
        factoryProductionSlider.value = factor;
    }

    // DESCRIPTION WAREHOUSE //

    void DisplayWarehouseInfos(HexaWareHouse warehouse)
    {
        warehouseInfos.text = "Maintenance : " + warehouse.maintenance.amount.ToString() + " / sec";
    }

    void PopulateWarehouseWorkers(HexaWareHouse warehouse)
    {
        int workerCount = 1;
        DeleteAllChildren(warehouseScrollContent.transform);
        foreach (HexaWorker worker in warehouse.Workers)
        {
            AddWorkerActivityBlockToContent(worker, prefabWorkerActivity, warehouseScrollContent, workerCount);
            workerCount++;
        }
    }

    void AddWorkerActivityBlockToContent(HexaWorker worker, GameObject prefab, GameObject content, int id)
    {
        GameObject child = Instantiate(prefab);
        child.transform.SetParent(content.transform, false);

        SetupWorkerActivityBlock(child, worker);
        child.GetComponent<WorkerActivityBox>().worker = worker;
    }

    void SetupWorkerActivityBlock(GameObject child, HexaWorker worker)
    {
        child.transform.GetChild(0).GetComponent<Text>().text = FormatWorkerActivity(worker);

        if (worker.IsCarryingResource())
        {
            child.transform.GetChild(1).gameObject.SetActive(true);
            child.transform.GetChild(2).gameObject.SetActive(true);
            child.transform.GetChild(1).GetComponent<Text>().text = worker.Carry.amount.ToString();
            child.transform.GetChild(2).GetComponent<Image>().sprite = worker.Carry.type.icon;
        }
        else
        {
            child.transform.GetChild(1).gameObject.SetActive(false);
            child.transform.GetChild(2).gameObject.SetActive(false);
        }
    }

    string FormatWorkerActivity(HexaWorker worker)
    {
        string activity = "";
        MovingState state = worker.Behavior.currentState;

        switch (state)
        {
            case MovingState.ON_WAY_TO_DESTINATION:
                if (worker.Behavior.destination)
                    activity += "Going to " + worker.Behavior.destination.name;
                break;
            case MovingState.WAITING_FOR_PATH:
                activity += "Waiting for instructions";
                break;
            case MovingState.PAUSED:
                activity += "Paused";
                break;
        }

        return activity;
    }

    void ResetFocusOnWorker()
    {
        GameObject focus = GameObject.FindWithTag("worker_focus");
        Destroy(focus);
    }

    // HOUSE //

    void SetHouseInfos(HexaHouse house)
    {
        String residentName = "Residents: " + house.Type.ResidentName + Environment.NewLine;
//        String taxes = "Taxes: " + house.Type.GoldProduction + Environment.NewLine;
        String taxes = "Revenues: " + HouseMaster.instance.RevenuesPerHouseTypePerHouse[house.Type].ToString("n1") + Environment.NewLine;
        String community = "Community buildings increases residents incomes" + Environment.NewLine;
        houseInfos.text = residentName + taxes + community;
    }

    void PopulateHouseNeeds(HexaHouse house)
    {
        DeleteAllChildren(houseNeedsContent.transform);
        Dictionary<string, Resource> totalNeeds = GetHouseNeeds(house);
        foreach (var item in totalNeeds)
            InstanciateHouseNeedBlock(item.Value);
        foreach (HexaAoEType aoEType in house.Type.Community)
            InstanciateHouseAoENeedBlock(aoEType);
    }

    Dictionary<string, Resource> GetHouseNeeds(HexaHouse house)
    {
        Dictionary<string, Resource> totalNeeds = new Dictionary<string, Resource>();
        foreach (HexaHouseType houseType in HouseMaster.instance.HousesDefinition)
        {
            foreach (Resource resource in houseType.Need)
            {
                if (totalNeeds.ContainsKey(resource.type.name))
                    totalNeeds[resource.type.name].amount += resource.amount;
                else
                    totalNeeds[resource.type.name] = new Resource(resource);
            }
            if (houseType.HouseLevel == house.Type.HouseLevel)
                return totalNeeds;
        }
        return null;
    }

    void InstanciateHouseAoENeedBlock(HexaAoEType aoEType)
    {
        GameObject block = Instantiate(prefabBlockResourceGauge);
        block.transform.SetParent(houseNeedsContent.transform, false);
        block.transform.GetChild(0).GetComponent<Image>().sprite = aoEType.icon;
        int efficiency = 0;
        foreach (HexaAoEBuilding aoEBuilding in TimeMaster.instance.Communities)
        {
            if (aoEBuilding.Type == aoEType && aoEBuilding.ScanRangeWithDistanceForSpecificHouse(house))
            {
                efficiency = 100;
                break;
            }
        }
		HelpText help = block.GetComponent<HelpText>();
		help.helpText = aoEType.CommunityName;
       
		Slider slider = block.transform.GetChild(1).GetComponent<Slider>();
		slider.value = efficiency;
		Image fill = slider.transform.GetChild(1).GetChild(0).GetComponent<Image>();
		fill.color = Color.Lerp(UIMaster.instance.colorPalet[(int)COLOR_PALET.RED], UIMaster.instance.colorPalet[(int)COLOR_PALET.GREEN], efficiency);
    }

    void InstanciateHouseNeedBlock(Resource resource)
    {
        GameObject block = Instantiate(prefabBlockResourceGauge);
        block.transform.SetParent(houseNeedsContent.transform, false);
        block.transform.GetChild(0).GetComponent<Image>().sprite = resource.type.icon;
//		block.transform.GetChild(1).GetComponent<Text>().text = resource.amount.ToString();
        int efficiency = 0;
        try
        {
            efficiency = HouseMaster.instance.GetResourceEfficiencyByType(resource.type);
        }
        catch (KeyNotFoundException)
        {
        }
		
		HelpText help = block.GetComponent<HelpText>();
		help.helpText = resource.type.name;

		Slider slider = block.transform.GetChild(1).GetComponent<Slider>();
		slider.value = efficiency;
		Image fill = slider.transform.GetChild(1).GetChild(0).GetComponent<Image>();
		fill.color = Color.Lerp(UIMaster.instance.colorPalet[(int)COLOR_PALET.RED], UIMaster.instance.colorPalet[(int)COLOR_PALET.GREEN], efficiency);
	}
}