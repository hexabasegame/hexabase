﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour {

	public string tutorialName;
	public List<TutorialStep> steps;

	int currentStepIndex = 0;

	public TutorialStep GetNextStep()
	{
		if (currentStepIndex >= steps.Count)
			return null;
		return steps [currentStepIndex++];
	}

	public void ResetTutorial() 
	{
		currentStepIndex = 0;
		foreach (TutorialStep step in steps) {
			step.ResetStep ();
		}
	}
}
