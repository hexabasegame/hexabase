﻿using UnityEngine;
using UnityEngine.UI;

public class ConstructionButton : MonoBehaviour {

	public HexaBuilding building = null;

	void Awake ()
	{
		if (!building)
			Debug.Log("You need to assign a building to the button");
		Image image = this.transform.GetChild(0).GetComponent<Image>();
		image.sprite = building.icon;
		this.gameObject.name = "Button Construct " + building.name;
		gameObject.GetComponent<HelpText>().helpText = building.name;
	}

	public void ButtonOnClick() 
	{	
		BuildMaster.instance.ResetChosenBuilding();
		BuildMaster.instance.ChooseBuilding (building);
	}
}
