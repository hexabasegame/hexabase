﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

class HexDirections {
	private static HexDirections instance = null;
	private readonly List<Dictionary<Direction, int[]>> _namedDirectionsEven;
	private readonly List<Dictionary<Direction, int[]>> _namedDirectionsOdd;

	public HexDirections() {
		_namedDirectionsEven = new List<Dictionary<Direction, int[]>> {
			new Dictionary<Direction, int[]> {{Direction.NW, new[] {1, -1}}},
			new Dictionary<Direction, int[]> {{Direction.NE, new[] {1, 0}}},
			new Dictionary<Direction, int[]> {{Direction.E, new[] {0, 1}}},
			new Dictionary<Direction, int[]> {{Direction.SE, new[] {-1, 0}}},
			new Dictionary<Direction, int[]> {{Direction.SW, new[] {-1, -1}}},
			new Dictionary<Direction, int[]> {{Direction.W, new[] {0, -1}}}
		};
		_namedDirectionsOdd = new List<Dictionary<Direction, int[]>> {
			new Dictionary<Direction, int[]> {{Direction.NW, new[] {1, 0}}},
			new Dictionary<Direction, int[]> {{Direction.NE, new[] {1, 1}}},
			new Dictionary<Direction, int[]> {{Direction.E, new[] {0, 1}}},
			new Dictionary<Direction, int[]> {{Direction.SE, new[] {-1, 1}}},
			new Dictionary<Direction, int[]> {{Direction.SW, new[] {-1, 0}}},
			new Dictionary<Direction, int[]> {{Direction.W, new[] {0, -1}}}
		};
	}

	public List<Dictionary<Direction, int[]>> NamedDirectionsEven {
		get { return _namedDirectionsEven; }
	}

	public List<Dictionary<Direction, int[]>> NamedDirectionsOdd {
		get { return _namedDirectionsOdd; }
	}

	public static HexDirections Instance {
		get {
			if (instance == null) {
				instance = new HexDirections();
			}
			return instance;
		}
	}
}

public class HexGrid : MonoBehaviour {
	public static HexGrid instance = null;

	private int width = HexMetrics.width;
	private int height = HexMetrics.height;

	public HexCell cellPrefab;
	public Text cellLabelPrefab;

	static HexCell[] cells;
	static GameObject[] hexaShapes;
	static Text[] labels;
	static HexCell[][] twoDCells;
	static Text[][] twoDLabels;
	Canvas gridCanvas;
	HexMesh hexMesh;
	private GameObject hexaShape;
	private GameObject grassShape;

	public Color defaultColor = Color.blue;

	public bool debug;

	public static HexCell[][] TwoDCells {
		get { return twoDCells; }
	}

	public GameObject HexaShape {
		get { return hexaShape; }
	}


	void Awake() {
		if (instance == null)
			instance = this;


		debug = SaveMaster.instance.debug;

		hexaShape = Resources.Load<GameObject>("HexagoneHalfShapeWhite");
		grassShape = Resources.Load<GameObject>("Grass");
		gridCanvas = GetComponentInChildren<Canvas>();
		hexMesh = GetComponentInChildren<HexMesh>();

		cells = new HexCell[height * width];
		hexaShapes = new GameObject[height * width];
		labels = new Text[height * width];

		for (int z = 0, i = 0; z < height; z++) {
			for (int x = 0; x < width; x++) {
				CreateCell(x, z, i++);
			}
		}
		TransformCellsArrayInCellsArrayOfArray();
	}

	void Start() {
		if (SaveMaster.instance.FilePath != null) {
			SaveMaster.instance.LoadGame();
		} else {
			TerrainSculptor.DoYourThing();
			TerrainTexturer.DoYourThing();
		}
	}


	void TransformCellsArrayInCellsArrayOfArray() {
		twoDCells = new HexCell[width][];
		twoDLabels = new Text[width][];
		for (int i = 0; i < width; i++) {
			TwoDCells[i] = new HexCell[height];
			twoDLabels[i] = new Text[height];
		}
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				TwoDCells[i][j] = cells[i * HexMetrics.height + j];
				twoDLabels[i][j] = labels[i * HexMetrics.height + j];
			}
		}
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				SetNeighboors(i, j);
			}
		}
	}

	void SetNeighboors(int i, int j) {
		HexCell cell = TwoDCells[i][j];
		List<Dictionary<Direction, int[]>> namedDirections;
		int[][] directions;
		if (i % 2 == 0) {
			namedDirections = HexDirections.Instance.NamedDirectionsEven;
		}
		else {
			namedDirections = HexDirections.Instance.NamedDirectionsOdd;
		}

		foreach (Dictionary<Direction, int[]> namedDirection in namedDirections) {
			foreach (KeyValuePair<Direction, int[]> keyValuePair in namedDirection) {
				int[] dir = keyValuePair.Value;
				if (i + dir[0] < 0 || i + dir[0] == HexMetrics.height
				    || j + dir[1] < 0 || j + dir[1] == HexMetrics.width) {
					continue;
				}
				cell.AddOrientedNeighboor(TwoDCells[i + dir[0]][j + dir[1]], keyValuePair.Key);
			}
		}
	}


	void CreateCell(int x, int z, int i) {
		Vector3 position;
		position.x = (x + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
		position.y = 0f;
		position.z = z * (HexMetrics.outerRadius * 1.5f);

		HexCell cell = cells[i] = new HexCell();
		cell.position = position;
		cell.index = i;
		cell.coordinates = HexCoordinates.FromOffsetCoordinates(x, z);

		if (debug) {
			InstanciateAnHexaShape(position, i);
		}

//	    GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
//	    go.transform.position = position;
//	    Vector3 scale = new Vector3(1.732050808f - 0.05f, 0.3f, 1.5f-0.05f);
//	    go.transform.localScale = scale;
	}

	public void InstanciateAnHexaShape(Vector3 position, int i) {
		Vector3 tmpPos = new Vector3(position.x, position.y + 0.1f, position.z);
		GameObject hexaShapeGo = hexaShapes[i] = Instantiate(HexaShape, tmpPos, HexaShape.transform.rotation);
	}


	public double GetRandomNumber(double minimum, double maximum) {
		var random = new Random();
		return random.NextDouble() * (maximum - minimum) + minimum;
	}

	public static int GetDistanceBetweenTwoCells(HexCell a, HexCell b) {
		return (Math.Abs(a.coordinates.X - b.coordinates.X) + Math.Abs(a.coordinates.Y - b.coordinates.Y) +
		        Math.Abs(a.coordinates.Z - b.coordinates.Z)) / 2;
	}

	public static HexCell[] GetCells() {
		return cells;
	}

	public static GameObject[] GetHexaShapes() {
		return hexaShapes;
	}

	public static Text[] GetLabels() {
		return labels;
	}
}
