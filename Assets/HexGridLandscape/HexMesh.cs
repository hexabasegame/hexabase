﻿using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class HexMesh : MonoBehaviour {
    Mesh hexMesh;
    List<Vector3> vertices;
    List<Vector3> normals;
    List<int> triangles;
    BoxCollider boxCollider;
    List<Color> colors;
	private MeshRenderer rend;
	public float scrollSpeed = 0.08F;
	private Random rand;

    void Awake() {
        GetComponent<MeshFilter>().mesh = hexMesh = new Mesh();
        vertices = new List<Vector3>();
        normals = new List<Vector3>();
        triangles = new List<int>();
        colors = new List<Color>();

	    
	    Vector3 newSize = new Vector3(
		    HexMetrics.width * 3,
		    0,
		    HexMetrics.height * 3
	    );

        CreateSquare(newSize);
	    rand = new Random();
    }

	void LateUpdate() {
		float xOffset = Time.time * scrollSpeed;
		rend.material.SetTextureOffset("_MainTex", new Vector2(xOffset, xOffset));
	}

	public void CreateSquare(Vector3 meshSize) {

	    float waterHeight = -0.1f;

		Vector2[] theUVs = new Vector2[6];

		Vector3 corner1 = new Vector3(-HexMetrics.width					, 	waterHeight, -HexMetrics.height);				//0, 0
		Vector3 corner2 = new Vector3(-HexMetrics.width					, 	waterHeight, meshSize.z);						//0, 1
		Vector3 corner3 = new Vector3(meshSize.x						,	waterHeight, -HexMetrics.height);				//1, 0

		Vector3 corner4 = new Vector3(meshSize.x						, 	waterHeight, meshSize.z);						//1, 1
		Vector3 corner5 = new Vector3(-HexMetrics.width					,	waterHeight, meshSize.z);						//0, 1
		Vector3 corner6 = new Vector3(meshSize.x						,	waterHeight, -HexMetrics.height);				//1, 0
        vertices.Add(corner1);
        vertices.Add(corner2);
        vertices.Add(corner3);
        vertices.Add(corner4);
        vertices.Add(corner5);
        vertices.Add(corner6);
        normals.Add(new Vector3(0, 1, 0));
        normals.Add(new Vector3(0, 1, 0));
        normals.Add(new Vector3(0, 1, 0));
        normals.Add(new Vector3(0, 1, 0));
        normals.Add(new Vector3(0, 1, 0));
        normals.Add(new Vector3(0, 1, 0));
        triangles.Add(0);
        triangles.Add(1);
        triangles.Add(2);
        triangles.Add(5);
        triangles.Add(4);
        triangles.Add(3);
		theUVs[0] = new Vector2( 0, 0 );
		theUVs[1] = new Vector2( 0, 1 );
		theUVs[2] = new Vector2( 1, 0 );
		theUVs[3] = new Vector2( 1, 1 );
		theUVs[4] = new Vector2( 0, 1 );
		theUVs[5] = new Vector2( 1, 0 );

	    float r = 0x1d;
	    float g = 0x33;
	    float b = 0x4a;
	    Color floorColor = new Color(r / 255f, g / 255f, b / 255f, 0.5f);

	    rend = GetComponent<MeshRenderer>();
	    Material water = Resources.Load<Material>("Material/Water");
	    rend.material = water;


        hexMesh.vertices = vertices.ToArray();
        hexMesh.triangles = triangles.ToArray();
        hexMesh.normals = normals.ToArray();
	    hexMesh.uv = theUVs;
		rend.material.mainTextureScale = new Vector2(120,120);
    }

}