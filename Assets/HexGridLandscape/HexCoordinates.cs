﻿using System;
using UnityEngine;

[Serializable]
public struct HexCoordinates {

	private readonly int baseX;
	private readonly int baseY;

	[SerializeField] private int x, z;

    public int X {
        get { return x; }
    }

    public int Z
    {
        get { return z; }
    }

    public int Y {
        get {
            return -X - Z;
        }
    }

	public int BaseX {
		get { return baseX; }
	}

	public int BaseY {
		get { return baseY; }
	}

	public static HexCoordinates FromOffsetCoordinates (int x, int z) {
        return new HexCoordinates(x - z / 2, z, x, z);
    }

	public HexCoordinates(int x, int z, int baseX, int baseY) {
		this.x = x;
		this.z = z;
		this.baseX = baseX;
		this.baseY = baseY;
	}

//    public HexCoordinates (int x, int z) {
//        this.x = x;
//        this.z = z;
//    }

    public override string ToString () {
        return "(" + X.ToString() + ", " + Y.ToString() + ", " + Z.ToString() + ")";
    }

    public string ToStringOnSeparateLines () {
        return X.ToString() + "\n" + Y.ToString() + "\n" + Z.ToString();
    }

    public static HexCoordinates FromPosition (Vector3 position) {
        float x = position.x / (HexMetrics.innerRadius * 2f);
        float y = -x;
        float offset = position.z / (HexMetrics.outerRadius * 3f);
        x -= offset;
        y -= offset;
        int iX = Mathf.RoundToInt(x);
        int iY = Mathf.RoundToInt(y);
        int iZ = Mathf.RoundToInt(-x -y);

        if (iX + iY + iZ != 0) {
            float dX = Mathf.Abs(x - iX);
            float dY = Mathf.Abs(y - iY);
            float dZ = Mathf.Abs(-x -y - iZ);

            if (dX > dY && dX > dZ) {
                iX = -iY - iZ;
            }
            else if (dZ > dY) {
                iZ = -iX - iY;
            }        }

        return new HexCoordinates(iX, iZ, iX, iY);
    }

    public bool Equals(HexCoordinates obj)
    {
        return this.x == obj.x && this.z == obj.z;
    }
}