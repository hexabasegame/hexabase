﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

public enum CellType {
	Grass,
	River,
	Beach,
	MountainFeet,
	MountainSide,
	Mountain,
	BuildingCenter,
	BuildingSide,
	Road
}

public enum Direction {
	NE,NW,W,SW,SE,E
}

public static class DirectionMethods {
	public static Direction GetNext(Direction d) {
		Direction res = Direction.NE;
		switch (d) {
			case Direction.NE:
				res = Direction.E;
				break;
			case Direction.E:
				res = Direction.SE;
				break;
			case Direction.SE:
				res = Direction.SW;
				break;
			case Direction.SW:
				res = Direction.W;
				break;
			case Direction.W:
				res = Direction.NW;
				break;
			case Direction.NW:
				res = Direction.NE;
				break;
		}
		return res;
	}

	public static Direction GetOpposite(Direction d) {
		Direction res = Direction.NE;
		switch (d) {
			case Direction.NE:
				res = Direction.SW;
				break;
			case Direction.E:
				res = Direction.W;
				break;
			case Direction.SE:
				res = Direction.NW;
				break;
			case Direction.SW:
				res = Direction.NE;
				break;
			case Direction.W:
				res = Direction.E;
				break;
			case Direction.NW:
				res = Direction.SE;
				break;
		}
		return res;
	}

	public static Direction RotateDirectionByDegree(Direction d, Quaternion q) {
		int i = Mathf.RoundToInt(q.eulerAngles.y / 60);
		Direction res = d;
		for (int j = 0; j < i; j++) {
			res = GetNext(res);
		}
		return res;
	}

	public static Direction FromString(string d) {
		switch (d) {
			case "NE":
				return Direction.NE;
			case "NW":
				return Direction.NW;
			case "W":
				return Direction.W;
			case "SW":
				return Direction.SW;
			case "SE":
				return Direction.SE;
			case "E":
				return Direction.E;
			default:
				return Direction.W;
		}
	}
}

public class OnTheBorderException : Exception {
	public OnTheBorderException(string message) : base(message) {}
}

public class HexCell {

	public HexCoordinates coordinates;
	public Color color;

	private HexaRenderable building = null;
	private List<HexCell> childs;
	private HexCell motherCell = null;
	private List<HexCell> _neighboors = new List<HexCell>();
	private Dictionary<Direction, HexCell> _directionNeighboors = new Dictionary<Direction, HexCell>();

    public int index;

    public Vector3 position;
    public GameObject gameObject;

	public CellType cellType = CellType.Grass;
	public CellType originalType = CellType.Grass;

	// Use this for initialization
	void Start() { }

	// Update is called once per frame
	void Update() { }

	public HexCell MotherCell {
		get {return motherCell;}
		set {motherCell = value;}
	}

	public HexaRenderable Building {
		get { return building; }
		set { building = value; }
	}

	public bool HasBuilding {
		get { return building != null; }
	}

	public List<HexCell> Neighboors {
		get { return _neighboors; }
		set { _neighboors = value; }
	}

	public List<HexCell> Childs {
		get { return childs; }
		set { childs = value; }
	}

	public void AddNeighboor(HexCell nb) {
		_neighboors.Add(nb);
	}

	public void AddOrientedNeighboor(HexCell nb, Direction d) {
		_directionNeighboors[d] = nb;
		AddNeighboor(nb);
	}

	public HexCell GetNeighboorByDirection(Direction d) {
		if (_directionNeighboors.ContainsKey(d)) {
			return _directionNeighboors[d];
		}
		throw new OnTheBorderException("This cell has no neighboor in direction "+d);
	}
}