﻿using System.Collections.Generic;
using UnityEngine;

public class HexaHouseType : MonoBehaviour {

	public string ResidentName;
	public int HouseLevel;
	public float ConsumptionInterval;
	public List<Resource> Need;
	public List<HexaAoEType> Community;
	public float GoldProduction;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public Dictionary<Resource, int> GetNeededResourceCountForHouseCount(int houseCount) {
		Dictionary<Resource, int> needs = new Dictionary<Resource, int>();
		foreach (Resource resource in Need) {
			needs[resource] = resource.amount * houseCount;
		}
		return needs;
	}

}
