﻿using System;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	private float minHheight = 13;
	private float maxHheight = 45;
	private float zoomSpeed = 10.0f;
	public float zoomMaxSpeed = 10f;
	public float zoomMinSpeed = 5.0f;
	private float zoomDiffSpeed = 4.57142857f;


	private int maxX = Mathf.RoundToInt(HexMetrics.height * HexMetrics.outerRadius * 2);

	private int maxZ = Mathf.RoundToInt(HexMetrics.width * HexMetrics.innerRadius * 2 - (HexMetrics.width * HexMetrics.outerRadius * 2) / 10) - 34;

	public bool freezeCamera = false;

	private Vector3 Origin;
	private Vector3 Difference;
	private bool Drag = false;

	public float speedH = 2.0f;
	public float speedV = 2.0f;
	public float keyboardMultiplier = 5.0f;


	private float yaw = 0.0f;
	private float pitch = 75f;

	void Awake() {
	}

	void Start() {
		pitch = Camera.main.transform.rotation.eulerAngles.x;
		yaw = Camera.main.transform.rotation.eulerAngles.y;
	}

	void LateUpdate ()
	{
		if (freezeCamera) {
			Drag = false;
			return;
		}

		if (Input.GetMouseButton(2)) {
			float height = Camera.main.transform.position.y;
			Vector3 worldClick = Camera.main.ScreenToWorldPoint(Input.mousePosition - new Vector3(0, 0, -height));
			Difference = (worldClick) - Camera.main.transform.position;
			if (Drag == false) {
				Drag = true;
				Origin = Camera.main.ScreenToWorldPoint(Input.mousePosition - new Vector3(0, 0, -height));
			}
			Camera.main.transform.position = NewPosition(Origin - Difference);
		} else {
			Drag = false;
		}
		if (Input.GetAxis("Mouse ScrollWheel") > 0) {
			ButtonZoomIn();
		}
		if (Input.GetAxis("Mouse ScrollWheel") < 0) {
			ButtonZoomOut();
		}
		if (Input.GetMouseButton(1)) {
			yaw += speedH * Input.GetAxis("Mouse X");
			pitch -= speedV * Input.GetAxis("Mouse Y");
//			Debug.Log("Pitch: "+pitch);
//			Debug.Log("Yaw: "+yaw);
			pitch = Mathf.Clamp(pitch, 50, 75);
//			yaw = Mathf.Clamp(yaw, -40, 40);
			transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
		}
		MoveWithKeyboard();
	}

	public void MoveWithKeyboard() {
		Vector3 pos = Camera.main.transform.position;
		float distRatio = (pos.y - minHheight) / (maxHheight - minHheight);
		zoomSpeed = ((distRatio * zoomDiffSpeed) + zoomMinSpeed) * keyboardMultiplier;
//		Debug.Log(String.Format("Zoomspeed = ((distRatio * zoomDiffSpeed) + zoomMinSpeed) * keyboardMultiplier"));
//		Debug.Log(String.Format("{0} = (({1} * {2}) + {3}) * {4}", zoomSpeed, distRatio, zoomDiffSpeed, zoomMinSpeed, keyboardMultiplier));
		float y = transform.position.y;
		if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
		{
			transform.Translate(new Vector3(zoomSpeed * Time.deltaTime,0,0));
		}
		if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
		{
			transform.Translate(new Vector3(-zoomSpeed * Time.deltaTime,0,0));
		}
		if(Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
		{
			transform.Translate(new Vector3(0,0,-zoomSpeed * Time.deltaTime));
		}
		if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
		{
			transform.Translate(new Vector3(0,0,zoomSpeed * Time.deltaTime));
		}
		transform.position = new Vector3(transform.position.x, y, transform.position.z);
		transform.position = NewPosition(transform.position);
	}

	public void ButtonZoomIn ()
	{
		zoomDiffSpeed = zoomMaxSpeed - zoomMinSpeed;
		Vector3 pos = Camera.main.transform.position;
		float distRatio = (pos.y - minHheight) / (maxHheight - minHheight);
		zoomSpeed = (distRatio * zoomDiffSpeed) + zoomMinSpeed;
		Camera.main.transform.position = new Vector3(pos.x, Mathf.Max(minHheight, pos.y - zoomSpeed), pos.z);
	}

	public void ButtonZoomOut ()
	{
		zoomDiffSpeed = zoomMaxSpeed - zoomMinSpeed;
		Vector3 pos = Camera.main.transform.position;
		float distRatio = (pos.y - minHheight) / (maxHheight - minHheight);
		zoomSpeed = (distRatio * zoomDiffSpeed) + zoomMinSpeed;
		Camera.main.transform.position = new Vector3(pos.x, Mathf.Min(maxHheight, pos.y + zoomSpeed), pos.z);
	}

	Vector3 NewPosition (Vector3 rawPosition)
	{
		float height = Camera.main.transform.position.y;
		return new Vector3(
			Mathf.Clamp(rawPosition.x, 0,  maxX),
			Mathf.Clamp(height, minHheight, maxHheight),
			Mathf.Clamp(rawPosition.z, 0, maxZ)
		);
	}


}
