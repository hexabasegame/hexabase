﻿using UnityEngine;

public class HexaResource : MonoBehaviour {

	public new string name;
	public Sprite icon;

	public override string ToString() {
		return name + ": ";
	}
}
