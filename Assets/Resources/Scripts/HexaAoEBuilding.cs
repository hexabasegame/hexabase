﻿using System.Collections.Generic;

public class HexaAoEBuilding : HexaBuilding
{

	public HexaAoEType Type;
	
	public int scanRange = 20; 
	
	// Use this for initialization
	void Start () {
		base._type = BuildingType.Community;
	}

	// Update is called once per frame
	void Update () {

	}

	public override void OnConstruction(HexCell cell) {
		base.OnConstruction(cell);
		_type = BuildingType.Community;
		TimeMaster.instance.AddBuilding(this);
//		HouseMaster.instance.RegisterANewHouseOfType(Type);
	}

	public override void OnDestroy() {
		base.OnDestroy();
		TimeMaster.instance.RemoveBuilding(this);
	}

	public bool ScanRangeWithDistanceForSpecificHouse(HexaHouse house)
	{
		if (HexGrid.GetDistanceBetweenTwoCells(house.GridPosition, _gridPosition) <= scanRange)
		{
			return true;
		}
		return false;
	}

}
