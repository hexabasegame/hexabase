﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeMaster : MonoBehaviour {
	private List<HexaFactory> _factories = new List<HexaFactory>();
	private List<HexaWareHouse> _wareHouses = new List<HexaWareHouse>();
	private List<HexaHouse> _houses = new List<HexaHouse>();
	private List<HexaAoEBuilding> _communities = new List<HexaAoEBuilding>();

	public static TimeMaster instance = null;
	public float workersTime;
	public int workersCapacity;
	public bool isFirstGame = true;
	public int totalMaintenanceCostPerSeconds = 0;

	void Awake() {
		if (instance == null) {
			instance = this;
		}
	}

	public List<HexaHouse> Houses {
		get { return _houses; }
	}
	
	public List<HexaFactory> Factories {
		get { return _factories; }
	}

	public List<HexaWareHouse> WareHouses {
		get { return _wareHouses; }
	}
	
	public List<HexaAoEBuilding> Communities {
		get { return _communities; }
	}

	// Use this for initialization
	void Start() {
		StartCoroutine("ApplyMaintenanceCost");
	}

	IEnumerator ApplyMaintenanceCost() {
		while (true) {
			totalMaintenanceCostPerSeconds = 0;
			foreach (HexaFactory factory in Factories) {
				ResourceMaster.instance.RemoveGlobalResourceAmount(factory.maintenance);
				totalMaintenanceCostPerSeconds += factory.maintenance.amount;
			}
			foreach (HexaWareHouse wareHouse in WareHouses) {
				ResourceMaster.instance.RemoveGlobalResourceAmount(wareHouse.maintenance);
				totalMaintenanceCostPerSeconds += wareHouse.maintenance.amount;
			}

			yield return new WaitForSecondsRealtime(1f);
		}
	}

	public void AddBuilding(HexaBuilding building)
	{
		switch (building.Type)
		{
			case BuildingType.Community:
				AddAoEBuilding((HexaAoEBuilding) building);
				break;
			case BuildingType.Factory:
				AddFactory((HexaFactory) building);
				break;
			case BuildingType.House:
				AddHouse((HexaHouse) building);
				break;
			case BuildingType.Warehouse:
				AddWareHouse((HexaWareHouse) building);
				break;
		}
		TierMaster.instance.UpdateTier();
	}
	
	public void RemoveBuilding(HexaBuilding building)
	{
		switch (building.Type)
		{
			case BuildingType.Community:
				RemoveAoEBuilding((HexaAoEBuilding) building);
				break;
			case BuildingType.Factory:
				RemoveFactory((HexaFactory) building);
				break;
			case BuildingType.House:
				RemoveHouse((HexaHouse) building);
				break;
			case BuildingType.Warehouse:
				RemoveWareHouse((HexaWareHouse) building);
				break;
		}
	}
	
	public void AddHouse(HexaHouse house)
	{
		Houses.Add(house);
	}

	public void RemoveHouse(HexaHouse house)
	{
		Houses.Remove(house);
	}
	
	public void AddFactory(HexaFactory factory) {
		Factories.Add(factory);
		ToggleWareHouseRescan();
		ToggleFactoriesRescanForFactories();
		ToggleFactoriesRescanForWarehouses();
	}

	public void RemoveFactory(HexaFactory factory) {
		Factories.Remove(factory);
		ToggleWareHouseRescan();
		ToggleFactoriesRescanForFactories();
		ToggleFactoriesRescanForWarehouses();
	}

	public void AddWareHouse(HexaWareHouse wareHouse) {
		WareHouses.Add(wareHouse);
		ToggleWareHouseRescan();
		ToggleFactoriesRescanForFactories();
		ToggleFactoriesRescanForWarehouses();
	}

	public void RemoveWareHouse(HexaWareHouse wareHouse) {
		WareHouses.Remove(wareHouse);
		ToggleWareHouseRescan();
		ToggleFactoriesRescanForFactories();
		ToggleFactoriesRescanForWarehouses();
	}

	public void AddAoEBuilding(HexaAoEBuilding aoeBuilding)
	{
		Communities.Add(aoeBuilding);
	}

	public void RemoveAoEBuilding(HexaAoEBuilding aoeBuilding)
	{
		Communities.Remove(aoeBuilding);
	}
	
	public void ToggleWareHouseRescan() {
		foreach (HexaWareHouse wareHouse in WareHouses) {
//			var watch = System.Diagnostics.Stopwatch.StartNew();
			wareHouse.ScanRangeWithDistance();
//			watch.Stop();
//			Debug.Log("Took: "+watch.ElapsedMilliseconds+"ms to get in range factories.");
		}
	}

	public void ToggleFactoriesRescanForFactories() {
		foreach (HexaFactory factory in Factories) {
//			var watch = System.Diagnostics.Stopwatch.StartNew();
			factory.ScanRangeWithDistanceForFactories();
//			watch.Stop();
//			Debug.Log("Took: "+watch.ElapsedMilliseconds+"ms to get in range factories.");
		}
	}

	public void ToggleFactoriesRescanForWarehouses() {
		foreach (HexaFactory factory in Factories) {
//			var watch = System.Diagnostics.Stopwatch.StartNew();
			factory.ScanRangeWithDistanceForWareHouse();
//			watch.Stop();
//			Debug.Log("Took: "+watch.ElapsedMilliseconds+"ms to get in range factories.");
		}
	}
	
//	public void ToggleAoERescanForHouses()
//	{
//		foreach (HexaAoEBuilding hexaAoEBuilding in Community)
//		{
//			hexaAoEBuilding.ScanRangeWithDistanceForHouses();
//		}
//	}
}

public class Timer
{
	float timer = 0;
	float timePassed = 0;

	public Timer (float timer)
	{
		this.timer = timer;
	}

	public bool IsTime(bool debug=false)
	{
		timePassed += Time.deltaTime;
		if (debug)
			Debug.Log("timer : " + timer.ToString());
		if (timePassed >= timer)
			return true;
		return false;
	}

	public void Reset()
	{
		timePassed = 0;
	}

	public float GetTimer() { return this.timer; }
}

