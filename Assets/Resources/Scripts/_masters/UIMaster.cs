﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResourceMaxCountException : Exception {}

public enum COLOR_PALET{
	RED = 1,
	BROWN = 4,
	GREEN = 7,
	GREY = 10,
	YELLOW = 13
}

public class UIMaster : MonoBehaviour {

	public static UIMaster instance = null;

	// color code
	public Color [] colorPalet;

	// panels references
	public GameObject panelHelpText;
	public GameObject panelResource;
    public GameObject panelConstruction;
	public GameObject panelConstructionButtons;
	public DescriptionPanel panelDescription;
	public PanelPause panelPause;
	public Text dashboardIncomes;
	public HelpText dashboardIncomesHelpText;

	// prefabs
	public GameObject blockResourcePrefab;
	public List<GameObject> constructionButtonsPrefab;
	public GameObject blockCostHelperPrefab;
	public GameObject alertBillBoard;
	public GameObject blockTier;
	public GameObject smokeParticule;

	// icons
	public Sprite costIconPrefab;

	// privates
	private Dictionary<string, GameObject> UIresources = new Dictionary<string, GameObject>();
	private int resourceCount = 0;
	private int resourceHorizontalMax = 10;

	private List<GameObject> constructionButtons = new List<GameObject>();

	private bool isSlidePanelOpen = false;

	public bool IsSlidePanelOpen {
		get {
			return isSlidePanelOpen;
		}
	}

	void Start ()
	{
		instance.panelHelpText.SetActive (false);
		CreateTierLayout ();
		UpdateTierLayout();
		StartCoroutine("RefreshUICoroutine", 1f);
	}

	// UI API //

	public void RefreshButtonsDisplay ()
	{
		UpdateTierLayout();
	}

	public void RegisterNewConstructionButton (GameObject buttonPrefab, GameObject tierPanel)
	{
		// instanciate button
		GameObject constructionButton = Instantiate(buttonPrefab) as GameObject;
		constructionButton.transform.SetParent(tierPanel.transform, false);

		// register
		constructionButtons.Add(constructionButton);
	}

	public void RegisterNewResource (string name, Sprite icon)
	{
		// enlarge panel if needed
		resourceCount += 1;
		if (resourceCount < resourceHorizontalMax)
			panelResource.GetComponent<GridLayoutGroup>().constraintCount = resourceCount;

		// instanciate
		GameObject go = Instantiate(blockResourcePrefab) as GameObject;
		go.transform.SetParent(panelResource.transform, false);
		string helpName = "Resource : " + name;
		go.name = helpName;

		// customize UI display block
		go.transform.GetChild(0).GetComponent<Image>().sprite = icon;
		go.transform.GetChild(1).GetComponent<Text>().text = "0";
		go.GetComponent<HelpText>().helpText = helpName;

		// register
		UIresources.Add(name, go);
	}

	public void RefreshResourceDiplay (string name, float value)
	{
		if (UIresources[name] == null)
			return;
		UIresources[name].transform.GetChild(1).GetComponent<Text>().text = value.ToString();
	}

	public void RefreshFactoryDescritptionResourcesDisplay ()
	{
		if (!panelDescription.isHidden)
			panelDescription.RefreshFactoryResourceDisplay();
	}

	public void SpawnSmokeOnCell (HexCell cell, float scaleFactor=1)
	{
		GameObject smoke = Instantiate(smokeParticule, cell.position, Quaternion.identity);
		smoke.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
		Destroy(smoke, 1.5f);
	}

	void CreateTierLayout ()
	{
		// create each tier layout
		for (int tier = 0; tier < TierMaster.instance.Tiers.Length; tier++) {
			GameObject tierPanel = GameObject.Instantiate(blockTier) as GameObject;
			tierPanel.transform.SetParent(panelConstructionButtons.transform, false);
			tierPanel.transform.SetAsFirstSibling ();
			tierPanel.name = "Panel Tier " + tier;
			tierPanel.transform.GetChild (0).gameObject.SetActive(false);

			// prepare help text to display requierments.
			HelpText helper = tierPanel.GetComponent<HelpText> ();
			helper.helpText = "Building requirements :";
			helper.tier = TierMaster.instance.Tiers[tier];

			// create all button inside the tier layout
			foreach (GameObject constructionButton in constructionButtonsPrefab) {
				HexaBuilding building = constructionButton.GetComponent<ConstructionButton> ().building;
				if (building.requiredTier.level == tier)
					RegisterNewConstructionButton(constructionButton, tierPanel);
			}
		}
	}

	void UpdateTierLayout()
	{
		foreach(Transform tier in panelConstructionButtons.transform) {
			int tierNumber = Int32.Parse(tier.gameObject.name.Substring(11));
			if (tierNumber <= TierMaster.instance.CurrentTier) {
				tier.gameObject.SetActive (true);
				tier.GetComponent<HelpText> ().enabled = false;
				foreach (Transform button in tier) {
					if (button.gameObject.name != "Panel Tier Upgrade") {
						button.gameObject.SetActive (true);
						UpdateConstructionButtonDisplay (button.gameObject);
					} else
						button.gameObject.SetActive (false);
				}
			} else if (tierNumber == TierMaster.instance.CurrentTier + 1) {
				tier.gameObject.SetActive (true);
				foreach (Transform button in tier) {
					if (button.gameObject.name == "Panel Tier Upgrade")
						button.gameObject.SetActive (true);
					else
						button.gameObject.SetActive (false);
				}
			} else
				tier.gameObject.SetActive (false);
		}
	}

	private IEnumerator RefreshUICoroutine (float waitTime)
	{
		while (true) {
			RefreshDashboard ();
			yield return new WaitForSeconds (waitTime);
		}
	}

	public void RefreshDashboard ()
	{
		RefreshIncomes();
	}

	void RefreshIncomes ()
	{
		float totalIncomes = 0;
		float totalMaintenance = (float)TimeMaster.instance.totalMaintenanceCostPerSeconds;
		float total = 0;

		foreach (KeyValuePair<HexaHouseType, float> entry in HouseMaster.instance.RevenuesPerHouseType) {
			totalIncomes += entry.Value;
		}

		total = totalIncomes - totalMaintenance;
		if (dashboardIncomesHelpText.isDisplaying)
			dashboardIncomesHelpText.DisplayText("+ " + totalIncomes.ToString("n1") + "  -" + totalMaintenance.ToString("n1"));
		if (total >= 0) {
			dashboardIncomes.text = "+ " + (total).ToString ("n1");
			dashboardIncomes.color = colorPalet [(int)COLOR_PALET.GREEN];
		} else {
			dashboardIncomes.text = total.ToString ("n1");
			dashboardIncomes.color = colorPalet [(int)COLOR_PALET.RED];
		} 
	}

	public void UpdateConstructionButtonDisplay (GameObject constructionButton)
	{
		if (constructionButton == null)
			return;
		HexaBuilding building = constructionButton.GetComponent<ConstructionButton> ().building;

		Image image = constructionButton.GetComponent<Image> ();
		Color color = colorPalet[(int)COLOR_PALET.BROWN];
		if (!ResourceMaster.instance.HasEnoughGlobalResources (building.cost))
			color = colorPalet [(int)COLOR_PALET.RED];
		image.color = color;
	}

	public void SetDisplayPauseMenu(bool value)
	{
		if (value) {
			panelPause.gameObject.SetActive (true);
			panelPause.Open ();
		}
		else
			panelPause.Close();
	}

	// UI BUTTONS //

	public void ButtonConstruction ()
	{
		if (!isSlidePanelOpen) {
			panelConstructionButtons.GetComponent<Animator> ().SetBool ("openPanel", true);
			isSlidePanelOpen = true;
			if (!HexaTutorialMaster.instance.skipTutorialMode)
				HexaTutorialMaster.instance.DisplayTutorial ("TutorialConstruction");
		} else {
			panelConstructionButtons.GetComponent<Animator> ().SetBool ("openPanel", false);
			BuildMaster.instance.ResetChosenBuilding();
			isSlidePanelOpen = false;
		}
	}

	public void ButtonResume()
	{
		InputMaster.instance.ResumeGame ();
	}

	public void ButtonQuit()
	{
		SceneManager.LoadScene ("Scene Start");
	}

    /* ******************************
    *** Description Panel Methods ***
    ****************************** */

    public void ShowCellDescription(HexCell cell) {
		panelDescription.Display(cell);			
    }

	public void CloseCellDescription ()
	{
		panelDescription.Hide();
	}

	/* ******************************
    *** HelpText Panel Methods ***
    ****************************** */

	public void DisplayBuildingHelperInfo (HexaBuilding building)
	{
		DisplayBuildingCostHelperInfo(building);
		DisplayBuildingMaintenanceHelperInfo(building);
	}
	
	public void DestroyBuilingHelperInfo ()
	{
		int count = 0;
		foreach (Transform child in panelHelpText.transform) {
			if (count > 0)  // do not destroy child 0 -> text field
				Destroy(child.gameObject);
			count ++;
		}
	}

	void DisplayBuildingCostHelperInfo (HexaBuilding building)
	{
		GameObject costBlock = Instantiate (blockCostHelperPrefab);
		costBlock.transform.SetParent (panelHelpText.transform, false);
		costBlock.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = costIconPrefab;

		foreach (Resource resource in building.cost) {
			GameObject resourceBlock = Instantiate(blockResourcePrefab);
			resourceBlock.transform.SetParent(costBlock.transform, false);
			resourceBlock.transform.GetChild(0).GetComponent<Image>().sprite = resource.type.icon;
			Text value = resourceBlock.transform.GetChild(1).GetComponent<Text>();
			value.text = resource.amount.ToString();
			if (!ResourceMaster.instance.HasEnoughGlobalResource(resource))
				value.color = colorPalet[(int)COLOR_PALET.RED];
		}
	}

	void DisplayBuildingMaintenanceHelperInfo (HexaBuilding building)
	{
		if (building.maintenance.amount == 0)
			return;
		GameObject costBlock = Instantiate (blockCostHelperPrefab);
		costBlock.transform.SetParent (panelHelpText.transform, false);

		GameObject resourceBlock = Instantiate(blockResourcePrefab);
		resourceBlock.transform.SetParent(costBlock.transform, false);
		resourceBlock.transform.GetChild(0).GetComponent<Image>().sprite = building.maintenance.type.icon;
		Text value = resourceBlock.transform.GetChild(1).GetComponent<Text>();
		value.text = building.maintenance.amount.ToString();

	}	

	public void DisplayTierRequirement(HexaTier tier) 
	{
		GameObject costBlock = Instantiate (blockCostHelperPrefab);
		costBlock.transform.SetParent (panelHelpText.transform, false);
		costBlock.transform.GetChild(0).gameObject.SetActive(false);

		foreach (TierRequirements requirement in tier.buildingCountRequirements) {
			GameObject resourceBlock = Instantiate(blockResourcePrefab);
			resourceBlock.transform.SetParent(costBlock.transform, false);
			resourceBlock.transform.GetChild(0).GetComponent<Image>().sprite = requirement.building.icon;
			Text value = resourceBlock.transform.GetChild(1).GetComponent<Text>();
			value.text = requirement.amount.ToString();
//			if (!ResourceMaster.instance.HasEnoughGlobalResource(resource))
//				value.color = colorPalet[(int)COLOR_PALET.RED];
		}
	}

    // PRIVATE METHOD //

	void Awake() {
		if (instance == null)
			instance = this;
	}
}
