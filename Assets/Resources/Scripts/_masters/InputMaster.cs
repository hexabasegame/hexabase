﻿using UnityEngine;
using UnityEngine.EventSystems;

public enum GAMESTATE{
	DEFAULT,
	CONTRUCTION_DD,
	PAUSE
}

public class InputMaster : MonoBehaviour {

	public static InputMaster instance = null;

	GAMESTATE gameState = GAMESTATE.DEFAULT;

	private HexCell start = null;

	void Awake ()
	{
		if (instance == null)
			instance = this;
	}

	void Update ()
	{

		if (gameState == GAMESTATE.DEFAULT) {

			if( Input.GetKeyDown( KeyCode.Escape ) )
				PauseGame ();

			if (RaycastMaster.instance == null) {
				Debug.Log("RaycastMaster.instance == null!!!!!!");
				Debug.Log("RaycastMaster.instance == null!!!!!!");
				Debug.Log("RaycastMaster.instance == null!!!!!!");
				Debug.Log("RaycastMaster.instance == null!!!!!!");
				Debug.Log("RaycastMaster.instance == null!!!!!!");
				Debug.Log("RaycastMaster.instance == null!!!!!!");
				Debug.Log("RaycastMaster.instance == null!!!!!!");
				return;
			}
			if (Input.GetMouseButtonDown (0) && RaycastMaster.instance.MouseOverCell != null) {
				HexCell cell = RaycastMaster.instance.MouseOverCell;
//				PathFinding(cell);
				if (cell.HasBuilding)
					BuildingSelection (cell);
				else
					BuildingDeselection();
			}
		} else if (gameState == GAMESTATE.CONTRUCTION_DD) {

			if (Input.GetMouseButtonDown (0) && !EventSystem.current.IsPointerOverGameObject ()) {
				BuildConstruction ();
//				UIMaster.instance.ButtonConstruction ();
			}
			if (Input.GetMouseButtonDown (1)) {
				ResetConstruction ();
				UIMaster.instance.ButtonConstruction ();
			}
			if (Input.GetKeyDown (KeyCode.R)) {
				RotateBuilding (BuildMaster.instance.DragAndDropBuilding);
			}

		} else if (gameState == GAMESTATE.PAUSE) {

			if( Input.GetKeyDown( KeyCode.Escape ) )
				ResumeGame ();

		}
	}

	void RotateBuilding(GameObject building)
	{
		building.transform.RotateAround (building.transform.position, Vector3.up, 60);
//		Debug.Log (BuildMaster.instance.DragAndDropBuilding.transform.localRotation);

	}

	void PauseGame()
	{
		gameState = GAMESTATE.PAUSE;
		UIMaster.instance.SetDisplayPauseMenu (true);
	}

	public void ResumeGame()
	{
		UIMaster.instance.SetDisplayPauseMenu(false);
	}

	void BuildingSelection(HexCell cell)
	{
		UIMaster.instance.ShowCellDescription(cell);
	}

	void BuildingDeselection()
	{
		UIMaster.instance.CloseCellDescription ();
	}

	void BuildConstruction ()
	{
		if (RaycastMaster.instance.MouseOverCell != null) {
			BuildMaster.instance.ConstructBuildingOnCell (BuildMaster.instance.SelectedBuilding,
				RaycastMaster.instance.MouseOverCell, BuildMaster.instance.DragAndDropBuilding.transform.localRotation);
			UIMaster.instance.SpawnSmokeOnCell(RaycastMaster.instance.MouseOverCell, (float)BuildMaster.instance.SelectedBuilding.renderableSize);

			// this part is messy but necessary for tutorial : it close the construction panel so the user will open it later and trigger the next tutorial step
			if (!HexaTutorialMaster.instance.skipTutorialMode && UIMaster.instance.IsSlidePanelOpen && 
				HexaTutorialMaster.instance.CurrentTutorial != null && 
				HexaTutorialMaster.instance.CurrentTutorial.tutorialName == "TutorialConstruction" &&
				HexaTutorialMaster.instance.CurrentStep.name == "ConstructionStep1")
				UIMaster.instance.ButtonConstruction ();
		}
	}

	void ResetConstruction()
	{
		BuildMaster.instance.ResetChosenBuilding ();
	}

	public GAMESTATE GameState {
		get {
			return gameState;
		}
		set {
			gameState = value;
		}
	}
}
