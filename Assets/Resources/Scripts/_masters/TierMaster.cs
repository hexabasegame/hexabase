﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TierMaster : MonoBehaviour
{
    private int _currentTier = 0;
	private HexaTier[] tiers;

    public static TierMaster instance = null;

    public int CurrentTier
    {
        get { return _currentTier; }
    }

	public HexaTier[] Tiers {
		get { return tiers; }
		set { tiers = value; }
	}


    void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    void Start()
    {
        this.tiers = Resources.LoadAll<HexaTier>("Hexa Buildings/Tier");
//        foreach (HexaTier tier in tiers)
//        {
//            Debug.Log("Loaded tier " + tier.level);
//        }
    }

    public void UpdateTier()
    {
        foreach (HexaTier tier in this.tiers)
        {
            if (tier.level <= this._currentTier) continue;
            bool readyToUpgrade = true;
            foreach (TierRequirements requirements in tier.buildingCountRequirements)
            {
                int cnt = 0;
                List<HexaBuilding> buildings = new List<HexaBuilding>();
                switch (requirements.building.Type)
                {
                    case BuildingType.Community:
                        buildings = TimeMaster.instance.Communities.Cast<HexaBuilding>().ToList();
                        break;
                    case BuildingType.Factory:
                        buildings = TimeMaster.instance.Factories.Cast<HexaBuilding>().ToList();
                        break;
                    case BuildingType.House:
                        buildings = TimeMaster.instance.Houses.Cast<HexaBuilding>().ToList();
                        break;
                    case BuildingType.Warehouse:
                        buildings = TimeMaster.instance.WareHouses.Cast<HexaBuilding>().ToList();
                        break;
                }
                foreach (HexaBuilding building in buildings)
                {
                    if (requirements.building.name == building.name)
                    {
                        cnt++;
                    }
                }
                if (cnt < requirements.amount)
                {
                    readyToUpgrade = false;
                    break;
                }
            }
            if (readyToUpgrade)
            {
                _currentTier++;
            }
            break;
        }       
    }
}
