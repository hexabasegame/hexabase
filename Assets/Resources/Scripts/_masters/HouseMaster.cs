﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseMaster : MonoBehaviour {

	public static HouseMaster instance;
	public List<HexaHouseType> HousesDefinition;
	private readonly Dictionary<HexaHouseType, int> _housesCount = new Dictionary<HexaHouseType, int>();
	public readonly Dictionary<HexaHouseType, float> RevenuesPerHouseType = new Dictionary<HexaHouseType, float>();
	public readonly Dictionary<HexaHouseType, float> RevenuesPerHouseTypePerHouse = new Dictionary<HexaHouseType, float>();
	private readonly Dictionary<Resource, int> _resourceEfficiency = new Dictionary<Resource, int>();

	public Dictionary<Resource, int> ResourceEfficiency
	{
		get { return _resourceEfficiency; }
	}

	public int GetResourceEfficiencyByType(HexaResource type)
	{
		foreach (KeyValuePair<Resource,int> keyValuePair in _resourceEfficiency)
		{
			if (keyValuePair.Key.type == type)
			{
				return keyValuePair.Value;
			}
		}
		throw new KeyNotFoundException();
	}
	
	void Awake() {
		if (instance == null) {
			instance = this;
		}
	}

	// Use this for initialization
	void Start () {
		foreach (HexaHouseType type in HousesDefinition) {
			_housesCount[type] = 0;
		}
		foreach (KeyValuePair<HexaHouseType,int> keyValuePair in _housesCount) {
			StartCoroutine("ConsumeResourceAndWait", keyValuePair);
		}
	}

	// Update is called once per frame
	void Update () {

	}

	public void RegisterANewHouseOfType(HexaHouseType type) {
		foreach (HexaHouseType hexaHouseType in HousesDefinition) {
			if (hexaHouseType.HouseLevel <= type.HouseLevel) {
				_housesCount[hexaHouseType] = _housesCount[hexaHouseType] + 1;
			}
		}
	}

	public void RemoveAHouseOfType(HexaHouseType type) {
		foreach (HexaHouseType hexaHouseType in HousesDefinition) {
			if (hexaHouseType.HouseLevel <= type.HouseLevel) {
				_housesCount[hexaHouseType] = _housesCount[hexaHouseType] - 1;
			}
		}
	}

	IEnumerator ConsumeResourceAndWait(KeyValuePair<HexaHouseType,int> keyValuePair) {
		LinkedList<Dictionary<Resource, int>> recentResourceEfficiency = new LinkedList<Dictionary<Resource, int>>();
		while (true) {
			yield return new WaitForSecondsRealtime(keyValuePair.Key.ConsumptionInterval);
			HexaHouseType key = keyValuePair.Key;
			int value = _housesCount[key];
			if (value == 0) {
				continue;
			}
			
			/*
			 * HANDLE THE RESOURCES PART OF CONSUMING
			 */
			float efficiency = 0;
			Dictionary<Resource, int> spendingNeeds = key.GetNeededResourceCountForHouseCount(value);
			Dictionary<Resource, int> spendedNeeds = new Dictionary<Resource, int>();
			foreach (KeyValuePair<Resource,int> spendingNeed in spendingNeeds) {
				spendedNeeds[spendingNeed.Key] = 0;
			}
			for (int i = 0; i < value; i++) {
				foreach (Resource resource in key.Need) {
					if (ResourceMaster.instance.HasEnoughGlobalResource(resource)) {
						ResourceMaster.instance.RemoveGlobalResourceAmount(resource);
						if (!spendedNeeds.ContainsKey(resource)) {
							spendedNeeds[resource] = 0;
						}
						spendedNeeds[resource] += resource.amount;
					}
				}
			}
			foreach (KeyValuePair<Resource, int> spendedNeed in spendedNeeds) {
//				Debug.Log("Need "+spendingNeeds[spendedNeed.Key]+" of "+spendedNeed.Key.type.name);
//				Debug.Log("Spent "+spendedNeed.Value+" of "+spendedNeed.Key.type.name);
				_resourceEfficiency[spendedNeed.Key] = Mathf.RoundToInt(((float)spendedNeed.Value / (float)spendingNeeds[spendedNeed.Key]) * 100f);
				recentResourceEfficiency.AddFirst(new Dictionary<Resource, int>(_resourceEfficiency));
				if (recentResourceEfficiency.Count > 15)
					recentResourceEfficiency.RemoveLast();
//				Debug.Log("Efficiency is: " + _resourceEfficiency[spendedNeed.Key] + " but should be " + spendedNeed.Value + " / " + spendingNeeds[spendedNeed.Key] + "* 100");
			}
			foreach (KeyValuePair<Resource, int> valuePair in _resourceEfficiency)
			{
				foreach (Dictionary<Resource,int> dictionary in recentResourceEfficiency)
				{
					foreach (KeyValuePair<Resource,int> pair in dictionary)
					{
//						Debug.Log("Efficiency for "+ pair.Key.type.name + " is: " + pair.Value);
						efficiency += pair.Value;
					}
				}
			}
			efficiency /= _resourceEfficiency.Count * recentResourceEfficiency.Count;
//			Debug.Log("Efficiency pre-community is:" + efficiency);
			int houseWithCommunityNeedFilled = 0;
			foreach (HexaHouse house in TimeMaster.instance.Houses)
			{
				if (house.Type == keyValuePair.Key)
				{
					foreach (HexaAoEType aoEType in house.Type.Community)
					{
						foreach (HexaAoEBuilding aoEBuilding in TimeMaster.instance.Communities)
						{
							if (aoEBuilding.Type == aoEType && aoEBuilding.ScanRangeWithDistanceForSpecificHouse(house))
							{
								houseWithCommunityNeedFilled++;
//								efficiency += (efficiency/10);
								break;
							}
						}
					}
				}
			}
//			Debug.Log("Efficiency post-community is:" + efficiency);
			int rev = Mathf.FloorToInt(key.GoldProduction * value * efficiency / 100);
			rev = Mathf.FloorToInt(rev + (houseWithCommunityNeedFilled * key.GoldProduction * 0.1f));
			ResourceMaster.instance.AddGoldAmount(rev);
			RevenuesPerHouseType[keyValuePair.Key] = rev / keyValuePair.Key.ConsumptionInterval;
			RevenuesPerHouseTypePerHouse[keyValuePair.Key] = (rev / value) / keyValuePair.Key.ConsumptionInterval;
		}
	}
}
