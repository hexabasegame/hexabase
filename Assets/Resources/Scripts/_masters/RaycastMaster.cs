﻿using UnityEngine;
using UnityEngine.EventSystems;

public class RaycastMaster : MonoBehaviour {
	public static RaycastMaster instance = null;
	public RaycastHit hit;

	private GameObject mouseOverGameObject = null;
	private HexCell mouseOverCell = null;

	void Awake() {
		if (instance == null)
			instance = this;
	}

	void Update() {
		Raycast();
	}

	void Raycast() {
		Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(inputRay, out hit)) {
			mouseOverGameObject = hit.transform.gameObject;
		}
		else
			mouseOverGameObject = null;
	}

	public HexCell MouseOverCell {
		get // avoid use in update(), process consumming
		{
			if (EventSystem.current.IsPointerOverGameObject() || !mouseOverGameObject)
				return null;
			Vector3 position = hit.point;
			HexCoordinates coordinates = HexCoordinates.FromPosition(position);
			int index = coordinates.X + coordinates.Z * HexMetrics.width + coordinates.Z / 2;
			if (index < 0 || index >= HexGrid.GetCells ().Length)
				return null;

			mouseOverCell = HexGrid.GetCells()[index];
			if (mouseOverCell.MotherCell != null)
				return mouseOverCell.MotherCell;
			return mouseOverCell;
		}
		set { mouseOverCell = value; }
	}
}