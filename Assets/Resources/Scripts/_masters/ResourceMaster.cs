﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Resource {

	public Resource() {}

	public Resource(HexaResource type, int amount) {
		this.type = type;
		this.amount = amount;
	}

	public HexaResource type;
    public int amount;
	[HideInInspector] public int stockAmount = 0;
	[HideInInspector] public int stockAmountMax = -1;

    public Resource(Resource old) {
        this.type = old.type;
        this.amount = old.amount;
        this.stockAmount = old.stockAmount;
        this.stockAmountMax = old.stockAmountMax;
    }

	public bool Equal(Resource r) {
		return r.type.name == this.type.name;
	}

}

[Serializable]
public class SavableResource {
    public int amount;
    public string name;
	public int stockAmount;
	public int stockAmountMax;
}


public class ResourceMaster : MonoBehaviour {

	public static ResourceMaster instance = null;
	private Dictionary<string, Resource> globalStocks = new Dictionary<string, Resource>();
	public List<Resource> startResouces;
	public Resource gold;
	public Dictionary<string, Resource> GlobalStocks {
		get {return globalStocks;}
		set {globalStocks = value;}
	}
	
	void Awake ()
	{
		if (instance == null)
			instance = this;
	}

	void Start() {
		foreach (Resource resource in startResouces)
			AddGlobalResourceAmount(resource);
	}

	public void RegisterResource (Resource newResource)
	{
		if (globalStocks.ContainsKey(newResource.type.name))
			return;
		globalStocks.Add(newResource.type.name, new Resource(newResource.type, 0));
		UIMaster.instance.RegisterNewResource(newResource.type.name, newResource.type.icon);
	}

	public bool HasEnoughGlobalResources (List<Resource> resources)
	{
		if (resources == null || resources.Count == 0 || resources.Count == 0) // no cost == free
			return true;
		try {
			foreach (Resource resource in resources) {
				if (globalStocks [resource.type.name].stockAmount < resource.amount)
					return false;
			}
			return true;
		} catch (KeyNotFoundException) {
			return false;
		}
	}

	public bool HasEnoughGlobalResource (Resource resource)
	{
		try {
			if (globalStocks [resource.type.name].stockAmount < resource.amount)
				return false;
			return true;
		} catch (KeyNotFoundException) {
			return false;
		}
	}

	public void RemoveGlobalResourceAmount(Resource resource) {
		RegisterResource(resource);
		globalStocks[resource.type.name].stockAmount -= resource.amount;
		UIMaster.instance.RefreshResourceDiplay(resource.type.name, globalStocks[resource.type.name].stockAmount);
	    UIMaster.instance.RefreshButtonsDisplay();
	}

	public void AddGlobalResourceAmount(Resource resource)
	{
		RegisterResource(resource);
		globalStocks[resource.type.name].stockAmount += resource.amount;
		UIMaster.instance.RefreshResourceDiplay(resource.type.name, globalStocks[resource.type.name].stockAmount);
	    UIMaster.instance.RefreshButtonsDisplay();
	}

	public void AddGoldAmount(int amount) {
		gold.amount = amount;
		AddGlobalResourceAmount(gold);
	}

    public void SetResourceAmountOnLoad(Resource resource) {
        RegisterResource(resource);
		globalStocks[resource.type.name].amount = resource.amount;
		globalStocks[resource.type.name].stockAmount = resource.stockAmount;
		globalStocks[resource.type.name].stockAmountMax = resource.stockAmountMax;
		UIMaster.instance.RefreshResourceDiplay(resource.type.name, globalStocks[resource.type.name].stockAmount);
    }

    public List<SavableResource> GetResourcesForSave() {
        List<SavableResource> res = new List<SavableResource>();
        foreach (Resource value in globalStocks.Values) {
            SavableResource tmp = new SavableResource {
                amount = value.amount,
                name = value.type.name,
				stockAmount = value.stockAmount,
				stockAmountMax = value.stockAmountMax
            };
            res.Add(tmp);
        }
        return res;
    }

    public Resource GetGlobalResource(Resource resource) {
//        Debug.Log("GetGlobalResource for: "+resource.type.name+" is "+globalStocks[resource.type.name].amount);
        return globalStocks[resource.type.name];
    }
}
