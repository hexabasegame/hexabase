﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;

public class SaveMaster : MonoBehaviour
{
    public static SaveMaster instance = null;

    public List<string> saveFiles = new List<string>();

    public bool debug = true;

    private string filePath;
    private readonly string extention = ".saves";
    private readonly string separator = Path.DirectorySeparatorChar.ToString();

    public string FilePath
    {
        get { return filePath; }
        set { filePath = Application.persistentDataPath + separator + value + extention; }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            string[] saves = Directory.GetFiles(Application.persistentDataPath, "*.saves");
            foreach (string s in saves)
            {
                string tmp = s.Substring(Application.persistentDataPath.Length + separator.Length);
                tmp = tmp.Substring(0, tmp.Length - extention.Length);
                saveFiles.Add(tmp);
            }
//            saveFiles.AddRange(saves);
        }
        DontDestroyOnLoad(this);
    }


    public void SaveGame(string filename)
    {
        FilePath = filename;
        Debug.Log("FilePath: " + FilePath);
        SaveData saveData = new SaveData();

        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(FilePath))
        {
            File.Delete(FilePath);
        }
        FileStream file = File.Open(FilePath, FileMode.CreateNew);

        foreach (HexCell cell in HexGrid.GetCells())
        {
            if (cell.Building != null)
            {
                saveData.renderableDatas.Add(new RenderableData(cell.Building.name, cell));
            }
        }
        saveData.GlobalResourceDatas = ResourceMaster.instance.GetResourcesForSave();

        saveData.TerrainHeights = HexaTerrainData.Instance.Heights;
        foreach (HexCell hexCell in HexaTerrainData.Instance.River)
        {
            saveData.Rivers.Add(hexCell.index);
        }
        foreach (HexCell hexCell in HexaTerrainData.Instance.Mountain)
        {
            saveData.MountainTops.Add(hexCell.index);
        }
        foreach (HexCell hexCell in HexaTerrainData.Instance.MountainSide)
        {
            saveData.MountainSides.Add(hexCell.index);
        }
        foreach (HexCell hexCell in HexaTerrainData.Instance.MountainFeet)
        {
            saveData.MountainFeets.Add(hexCell.index);
        }
        foreach (HexCell hexCell in HexaTerrainData.Instance.Roads)
        {
            saveData.Roads.Add(hexCell.index);
        }

        Debug.Log("Saving: \n" + saveData.ToString());

        bf.Serialize(file, saveData);
        file.Close();
        FilePath = null;
    }

    public void LoadGame()
    {
        if (!File.Exists(FilePath))
        {
            Debug.Log("File [" + FilePath + "] doesn't exist");
            return;
        }
        Debug.Log(FilePath);

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(FilePath, FileMode.OpenOrCreate);

        SaveData data = (SaveData) bf.Deserialize(file);
        Debug.Log("Loading: \n" + data.ToString());
        file.Close();

        foreach (RenderableData buildingData in data.renderableDatas)
        {
            String[] Folders = new[] {"Factories", "Habitats", "AoE"};
            HexaBuilding toBuild = null;
            foreach (string folder in Folders)
            {
                toBuild = Resources.Load<HexaBuilding>("Hexa Buildings/" + folder + "/" + buildingData.prefabName);
                if (toBuild != null) break;
            }
            BuildMaster.instance.LoadBuildingOnCell(toBuild, HexGrid.GetCells()[buildingData.cellIndex], buildingData);
        }
        
        
        foreach (SavableResource resource in data.GlobalResourceDatas)
        {
            Resource tmp = new Resource
            {
                type = new HexaResource {name = resource.name},
                amount = resource.amount,
                stockAmount = resource.stockAmount,
                stockAmountMax = resource.stockAmountMax,
            };
            ResourceMaster.instance.SetResourceAmountOnLoad(tmp);
        }

        ResetTerrainData();
        foreach (int i in data.Roads)
            HexaTerrainData.Instance.Roads.Add(HexGrid.GetCells()[i]);
        TerrainSculptor.DoYourThingOnLoad(data.TerrainHeights, data.Rivers, data.MountainTops, data.MountainSides, data.MountainFeets);
        TerrainTexturer.DoYourThing();
        this.FilePath = null;
    }

    public void DeleteGame(string filename)
    {
        FilePath = filename;
        saveFiles.Remove(filename);
        File.Delete(FilePath);
        this.FilePath = null;
    }

    public void ResetTerrainData()
    {
        HexaTerrainData.Instance.River = new List<HexCell>();
        HexaTerrainData.Instance.Mountain = new List<HexCell>();
        HexaTerrainData.Instance.MountainSide = new List<HexCell>();
        HexaTerrainData.Instance.MountainFeet = new List<HexCell>();
        HexaTerrainData.Instance.Roads = new List<HexCell>();
    }
}



[Serializable]
public class SaveData
{
    public List<RenderableData> renderableDatas = new List<RenderableData>();
    public List<SavableResource> GlobalResourceDatas = new List<SavableResource>();

    public float[,] TerrainHeights =
        new float[HexaTerrainData.Instance.TerrainResolution, HexaTerrainData.Instance.TerrainResolution];

    public List<int> Rivers = new List<int>();
    public List<int> MountainTops = new List<int>();
    public List<int> MountainSides = new List<int>();
    public List<int> MountainFeets = new List<int>();
    public List<int> Roads = new List<int>();
    
    public new string ToString()
    {
        StringBuilder str = new StringBuilder();
        foreach (RenderableData data in renderableDatas)
        {
            str.Append("A ");
            str.Append(data.active ? "active" : "deactivated");
            str.Append(" building of type: ");
            str.Append(data.prefabName);
            str.Append(" will be build at coordinate: ");
            str.Append(data.coordinate.ToString());
            str.Append(" on cell with index [");
            str.Append(data.cellIndex);
            str.Append("].");
            str.Append("\n");
        }
        foreach (SavableResource resource in GlobalResourceDatas)
        {
            str.Append("A resource named: ");
            str.Append(resource.name);
            str.Append(" with amount: ");
            str.Append(resource.amount);
            str.Append(" and stock amount ");
            str.Append(resource.stockAmount);
            str.Append(" must be restored");
            str.Append("\n");
        }
        str.Append(String.Format("The Terrain Has Been saved in a {0} x {0} sized grid.\n", TerrainHeights.Length));
        str.Append(String.Format("{0} river cell has been saved.\n", Rivers.Count));
        return str.ToString();
    }

//    public List<int> vData = new List<int>();
}

[Serializable]
public class Vector3Data
{
    public float x;
    public float y;
    public float z;

    public Vector3Data(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3Data(Vector3 vector)
    {
        this.x = vector.x;
        this.y = vector.y;
        this.z = vector.z;
    }

    public Vector3 ToVector3()
    {
        return new Vector3(x, y, z);
    }
}

[Serializable]
public class RenderableData
{
    // Cell data
    public HexCoordinates coordinate;

    public int cellIndex;

    // Renderable data
    public string prefabName;

    public Vector3Data modelRotation;

    // building data


    // factory data
    public List<SavableResource> savedResources = new List<SavableResource>();

    public bool active;
    public float processTime;
    public float timeSpent;
    public int inputStockMax;
    public int outputStockMax;
    public FactoryState currentState;
    public FactoryState saveState;

    // PUBLIC METHODS //

    public RenderableData(string prefabName, HexCell cell)
    {
        Save(cell, prefabName);
    }

    public void Save(HexCell cell, string prefab)
    {
        SaveCellData(cell);
        if (cell.Building is HexaRenderable)
            SaveRederableData((HexaRenderable) cell.Building, prefab);
        if (cell.Building is HexaBuilding)
            SaveBuildingData((HexaBuilding) cell.Building);
        if (cell.Building is HexaFactory)
            SaveFactoryData((HexaFactory) cell.Building);
    }

    public HexaRenderable Load(HexaRenderable renderable)
    {
        LoadRenderableData(renderable);
        if (renderable is HexaBuilding)
            LoadBuildingData((HexaBuilding) renderable);
        if (renderable is HexaFactory)
            LoadFactoryData((HexaFactory) renderable);
        return renderable;
    }

    // SAVE METHODS //

    void SaveCellData(HexCell cell)
    {
        coordinate = cell.coordinates;
        cellIndex = cell.index;
    }

    void SaveRederableData(HexaRenderable renderable, string prefabName)
    {
        this.prefabName = prefabName;
        this.modelRotation = new Vector3Data(renderable.Model.transform.localRotation.eulerAngles);
    }

    void SaveBuildingData(HexaBuilding building)
    {
    }

    void SaveFactoryData(HexaFactory factory)
    {
        active = factory.IsActive;
        processTime = factory.processTime;
        timeSpent = factory.TimeSpent;
        inputStockMax = factory.InputStockMax;
        outputStockMax = factory.OutputStockMax;
        currentState = factory.CurrentState;
        saveState = factory.SaveState;

        SaveFactoryResources(factory.inputResource);
        SaveFactoryResources(factory.outputResource);
    }

    void SaveFactoryResources(List<Resource> factoryResources)
    {
        foreach (Resource resource in factoryResources)
        {
            SavableResource savedResource = new SavableResource
            {
                amount = resource.amount,
                name = resource.type.name,
                stockAmount = resource.stockAmount,
                stockAmountMax = resource.stockAmountMax
            };
            savedResources.Add(savedResource);
        }
    }

    // LOAD METHODS //

    void LoadRenderableData(HexaRenderable renderable)
    {
        renderable.Model.transform.localRotation = Quaternion.Euler(modelRotation.ToVector3());
    }

    void LoadBuildingData(HexaBuilding building)
    {
    }

    void LoadFactoryData(HexaFactory factory)
    {
        factory.IsActive = active;
        factory.processTime = processTime;
        factory.TimeSpent = timeSpent;
        factory.InputStockMax = inputStockMax;
        factory.OutputStockMax = outputStockMax;
        factory.CurrentState = currentState;
        factory.SaveState = saveState;

        LoadFactoryResources(factory.inputResource);
        LoadFactoryResources(factory.outputResource);
    }

    void LoadFactoryResources(List<Resource> factoryResources)
    {
        foreach (Resource factoryResource in factoryResources)
        {
            foreach (SavableResource savedResource in savedResources)
            {
                if (savedResource.name == factoryResource.type.name)
                {
                    factoryResource.amount = savedResource.amount;
                    factoryResource.stockAmount = savedResource.stockAmount;
                    factoryResource.stockAmountMax = savedResource.stockAmountMax;
                }
            }
        }
    }
}