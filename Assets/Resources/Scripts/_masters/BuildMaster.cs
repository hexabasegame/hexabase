﻿using System.Collections.Generic;
using UnityEngine;

public class BuildMaster : MonoBehaviour {
	public static BuildMaster instance = null;

	public Gridhelper gridHelper;

	List<HexaBuilding> buildings = new List<HexaBuilding>();
	HexaBuilding selectedBuilding = null;
	GameObject dragAndDropBuilding = null;
	GameObject dragAndDropEntrance = null;

	public bool buildings_paint_in_grey = false;

	void Awake() {
		if (instance == null)
			instance = this;
		gridHelper.gameObject.SetActive (false);
	}

	void Update() {
		if (dragAndDropBuilding != null) {
			DragAndDropPosition ();
			UpdateAoeBuildingColor();
			if (SelectedBuilding.CanBeConstructed())
				ChangeObjectAndChildColor(dragAndDropBuilding, UIMaster.instance.colorPalet[(int) COLOR_PALET.GREEN]);
			else
				ChangeObjectAndChildColor(dragAndDropBuilding, UIMaster.instance.colorPalet[(int) COLOR_PALET.RED]);
			DisplayEntrance();
		}
	}

	void DisplayEntrance() {
//		private void FindCreateEntrance(HexCell cell, HexaBuilding buildingPrefab, Quaternion rotation) {
		HexCell entrance = RaycastMaster.instance.MouseOverCell;
		if (entrance != null) {
			Direction entranceDirection = DirectionMethods.RotateDirectionByDegree(selectedBuilding.EntraceDirection, selectedBuilding.transform.rotation);
			for (int i = 0; i < selectedBuilding.renderableSize - 1; i++) {
				entrance = entrance.GetNeighboorByDirection(entranceDirection);
			}
//			dragAndDropEntrance.transform.position = entrance.position;
			dragAndDropEntrance.transform.position = new Vector3(entrance.position.x, entrance.position.y + 1, entrance.position.z);
		}
	}

	void DragAndDropPosition(){
		if (RaycastMaster.instance.MouseOverCell != null) {
			Vector3 currentPosition = RaycastMaster.instance.MouseOverCell.position;
			dragAndDropBuilding.transform.position = currentPosition;
			gridHelper.transform.position = currentPosition;
		} else {
			Vector3 lastValidPosition = new Vector3(RaycastMaster.instance.hit.point.x, 0, RaycastMaster.instance.hit.point.z);
			dragAndDropBuilding.transform.position = lastValidPosition;
			gridHelper.transform.position = lastValidPosition;
		}
	}

	void ChangeObjectAndChildColor(GameObject go, Color color) 
	{
		// parent
		Renderer parentRenderer = go.GetComponent<Renderer>();
		if (parentRenderer)
			ChangeRendererAllMaterialstColor(parentRenderer, color);

		// children
		Renderer[] childrenRenderers = go.transform.GetComponentsInChildren<Renderer>();
		for (int i = 0; i < childrenRenderers.Length; i++)
			ChangeRendererAllMaterialstColor(childrenRenderers[i], color);
	}

	void ChangeRendererAllMaterialstColor(Renderer rend, Color color) 
	{
		Material[] newMaterials = rend.materials;
		for (int i = 0; i < newMaterials.Length; i++)
			newMaterials[i].color = color;
		rend.materials = newMaterials;
	}

	public void LoadBuildingOnCell(HexaBuilding buildingPrefab, HexCell cell, RenderableData saveData)
	{
		SelectedBuilding = buildingPrefab;
		HexaBuilding building = ConstructBuildingOnCell(buildingPrefab, cell, true);
		saveData.Load((HexaRenderable) building);
	}

	public HexaBuilding ConstructBuildingOnCell(HexaBuilding buildingPrefab, HexCell cell, Quaternion rotation) {
		return ConstructBuildingOnCell(buildingPrefab, cell, false, rotation);
	}

	public HexaBuilding ConstructBuildingOnCell(HexaBuilding buildingPrefab, HexCell cell) {
		return ConstructBuildingOnCell(buildingPrefab, cell, false, Quaternion.identity);
	}

	public HexaBuilding ConstructBuildingOnCell(HexaBuilding buildingPrefab, HexCell cell, bool bypassCheck) {
		return ConstructBuildingOnCell(buildingPrefab, cell, bypassCheck, Quaternion.identity);
	}

	private void MoveAndColorCell(HexCell nextNeighboor) {
		if (SaveMaster.instance.debug) {
			GameObject hexaShape = HexGrid.GetHexaShapes()[nextNeighboor.index];
//		hexaShape.transform.position += new Vector3(0,0.1f,0);
			SpriteRenderer hexaRenderer = hexaShape.GetComponent<SpriteRenderer>();
			hexaRenderer.color = Color.blue;
		}
	}

	private void ResetAndUncolorCell(HexCell nextNeighboor) {
		if (SaveMaster.instance.debug) {
			GameObject hexaShape = HexGrid.GetHexaShapes()[nextNeighboor.index];
//		hexaShape.transform.position += new Vector3(0,-0.1f,0);
			SpriteRenderer hexaRenderer = hexaShape.GetComponent<SpriteRenderer>();
			hexaRenderer.color = Color.white;
		}
	}

	private void FindCreateEntrance(HexCell cell, HexaBuilding buildingPrefab, Quaternion rotation) {
		HexCell entrance = cell;
		Direction entranceDirection = DirectionMethods.RotateDirectionByDegree(buildingPrefab.EntraceDirection, rotation);
		for (int i = 0; i < buildingPrefab.renderableSize - 1; i++) {
			entrance = entrance.GetNeighboorByDirection(entranceDirection);
		}
		((HexaBuilding) cell.Building).Entrace = entrance;
		((HexaBuilding) cell.Building).EntraceDirection = entranceDirection;
	}

	public HexaBuilding ConstructBuildingOnCell(HexaBuilding buildingPrefab, HexCell cell, bool bypassCheck,
		Quaternion rotation) {
		if (!SelectedBuilding.CanBeConstructed() && !bypassCheck)
			return null;
		HexaBuilding building = Instantiate(buildingPrefab, cell.position, buildingPrefab.transform.rotation) as HexaBuilding;
		building.OnConstruction(cell);
		cell.Building = building;
		cell.cellType = CellType.BuildingCenter;
		cell.Childs = new List<HexCell>();
		HexaTerrainData.Instance.RemoveTreeFromCell(cell);
		if (building.name == "Stone Cutter") {
			TerrainSculptor.SetSingleCellElevationOnTerrain(cell, HexaTerrainData.GroundLevel);
		}

		List<HexCell> extractNeighboorFrom = new List<HexCell> {cell};
		List<HexCell> neighboorToLock = new List<HexCell>{cell};

		while (true) {
			HexCell current = neighboorToLock[0];
			if (HexGrid.GetDistanceBetweenTwoCells(current, cell) == building.renderableSize - 1) {
				break;
			}
			neighboorToLock.RemoveAt(0);
			foreach (HexCell nextNeighboor in current.Neighboors) {
				if (nextNeighboor.Building != null) {
					continue;
				}
				if ( !extractNeighboorFrom.Contains(nextNeighboor)) {
					neighboorToLock.Add(nextNeighboor);
					nextNeighboor.MotherCell = cell;
					nextNeighboor.cellType = CellType.BuildingSide;
					cell.Childs.Add(nextNeighboor);
					MoveAndColorCell(nextNeighboor);
					extractNeighboorFrom.Add(nextNeighboor);
					HexaTerrainData.Instance.RemoveTreeFromCell(nextNeighboor);
					if (building.name == "Stone Cutter") {
						TerrainSculptor.SetSingleCellElevationOnTerrain(nextNeighboor, HexaTerrainData.GroundLevel);
					}
				}
			}
		}

		FindCreateEntrance(cell, buildingPrefab, rotation);

		buildings.Add(building);
//		TimeMaster.instance.ToggleWareHouseRescan();
		if (typeof(HexaWareHouse) == building.GetType()) {
			TimeMaster.instance.ToggleFactoriesRescanForWarehouses();
		} else if (typeof(HexaFactory) == building.GetType()){
			TimeMaster.instance.ToggleFactoriesRescanForFactories();
		}

		return building;
	}

	public void DestroyBuilding (HexaRenderable building, HexCell cell)
	{
		cell.Building = null;
		cell.cellType = cell.originalType;
		foreach (HexCell neighboor in cell.Childs) {
			neighboor.MotherCell = null;
			neighboor.cellType = neighboor.originalType;
			ResetAndUncolorCell (neighboor);
		}
		cell.Childs = null;
		if (building is HexaFactory) {
			TimeMaster.instance.RemoveBuilding ((HexaFactory)building);
		} else if (building is HexaWareHouse) {
			TimeMaster.instance.RemoveBuilding ((HexaWareHouse)building);
		}
		Destroy(building.gameObject);
	}

	public void ChooseBuilding (HexaBuilding building)
	{
//		print ("ChooseBuilding");
		InputMaster.instance.GameState = GAMESTATE.CONTRUCTION_DD;
		SelectedBuilding = building;

		dragAndDropBuilding = Instantiate (building.modelPrefab) as GameObject;
		dragAndDropEntrance = Instantiate (HexGrid.instance.HexaShape) as GameObject;
		Vector3 currentScale = dragAndDropBuilding.transform.localScale;
		dragAndDropBuilding.transform.localScale = currentScale + new Vector3 (0.1f, 0.1f, 0.1f);

		// set up the help grid
		DragAndDropPosition ();
		gridHelper.gameObject.SetActive (true);
		gridHelper.GenerateScanRangeBuildingGrid (building, dragAndDropBuilding.transform.position, true);


		// handle paint building in grey if House is selected or not
		if (building is HexaHouse) {
			PaintAllBuildingsForAoeHouse();
		} else if (buildings_paint_in_grey == true) {
			PaintAllBuildingBackToOriginalColor();
		}
	}

	public void ResetChosenBuilding() {
//		print ("ResetChosenBuilding");
		if (buildings_paint_in_grey == true) 
			PaintAllBuildingBackToOriginalColor();
		Destroy(dragAndDropBuilding);
		Destroy(dragAndDropEntrance);
		gridHelper.gameObject.SetActive (false);
		dragAndDropBuilding = null;
		dragAndDropEntrance = null;
		SelectedBuilding = null;
		InputMaster.instance.GameState = GAMESTATE.DEFAULT;
	}

	public HexaBuilding SelectedBuilding {
		get { return selectedBuilding; }
		set { selectedBuilding = value; }
	}

	public GameObject DragAndDropBuilding {
		get { return dragAndDropBuilding; }
	}

	void PaintAllBuildingsForAoeHouse ()
	{
		Color color =  UIMaster.instance.colorPalet[((int) COLOR_PALET.GREY)-1];
		buildings_paint_in_grey = true;
		foreach (HexaBuilding building in TimeMaster.instance.Factories)
			ChangeObjectAndChildColor(building.gameObject, color);
		foreach (HexaBuilding building in TimeMaster.instance.Houses)
			ChangeObjectAndChildColor(building.gameObject, color);
		foreach (HexaBuilding building in TimeMaster.instance.WareHouses)
			ChangeObjectAndChildColor(building.gameObject, color);
		UpdateAoeBuildingColor();
	}

	void PaintAllBuildingBackToOriginalColor ()
	{
		buildings_paint_in_grey = false;
		foreach (HexaBuilding building in TimeMaster.instance.Factories)
			ColorObjectBackToOriginal(building);
		foreach (HexaBuilding building in TimeMaster.instance.Houses)
			ColorObjectBackToOriginal(building);
		foreach (HexaBuilding building in TimeMaster.instance.WareHouses)
			ColorObjectBackToOriginal(building);
		foreach (HexaBuilding building in TimeMaster.instance.Communities)
			ColorObjectBackToOriginal(building);
	}

	void UpdateAoeBuildingColor ()
	{
		if (buildings_paint_in_grey == false)
			return;
		HexCell currentCell = RaycastMaster.instance.MouseOverCell;
		if (currentCell == null)
			return;
		foreach (HexaAoEBuilding community in TimeMaster.instance.Communities) {

			if (HexGrid.GetDistanceBetweenTwoCells(currentCell, community.GridPosition) <= community.scanRange) {
				ChangeObjectAndChildColor(community.gameObject, UIMaster.instance.colorPalet[(int) COLOR_PALET.GREEN]);
			} else {
				ChangeObjectAndChildColor(community.gameObject,UIMaster.instance.colorPalet[(int) COLOR_PALET.RED]);
			}
		}
	}

	void ColorObjectBackToOriginal (HexaBuilding building)
	{
		GameObject originalGo = building.modelPrefab;
		
		Renderer rootRenderer = building.Model.GetComponent<Renderer> ();
		Renderer rootOriginalRend = originalGo.GetComponent<Renderer>();
		if (rootRenderer != null && rootOriginalRend != null) {
			CopyRendererColor(rootRenderer, rootOriginalRend);
		}

		for (int index = 0; index < building.Model.transform.childCount; index++) {
			Renderer currRenderer = building.Model.transform.GetChild (index).GetComponent<Renderer> ();
			Renderer originalRenderer = originalGo.transform.GetChild(index).GetComponent<Renderer>();
			if (currRenderer != null && originalRenderer != null) {
				CopyRendererColor(currRenderer, originalRenderer);
			}
		}
	}

	void CopyRendererColor (Renderer target, Renderer source)
	{
		Material[] newMaterials = target.sharedMaterials;
		for (int i = 0; i < newMaterials.Length; i++)
			if (source.sharedMaterials[i] && source.sharedMaterials[i].HasProperty("_Color"))
				newMaterials[i].color = source.sharedMaterials[i].color;
		target.materials = newMaterials;
	}

}