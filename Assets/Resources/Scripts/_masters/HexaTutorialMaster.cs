﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HexaTutorialMaster : MonoBehaviour {

	public static HexaTutorialMaster instance = null;

	public bool skipTutorialMode;
	public List<Tutorial> tutorials;

	// UI references
	public GameObject panelTutorialPopup;
	public GameObject panelTutorial;
	public Text title;
	public Image image;
	public Text explanations;
	public GameObject navigationLayout;
	public GameObject okButton;
	public GameObject prevButton;
	public GameObject nextButton;
	public GameObject pagesLayout;

	// prefab references
	public GameObject emptyDotPrefab;
	public GameObject filledDotPrefab;

	// privates
	Tutorial currentTutorial = null;
	TutorialStep currentStep = null;

	public TutorialStep CurrentStep {
		get {
			return currentStep;
		}
	}

	public Tutorial CurrentTutorial {
		get {
			return currentTutorial;
		}
	}

	// public methods

	public void DisplayTutorial (string tutorialName)
	{
		if (skipTutorialMode == true)
			return;

		foreach (Tutorial tutorial in tutorials) {
			if (tutorial.tutorialName == tutorialName) {
				currentTutorial = tutorial;
				currentStep = tutorial.GetNextStep();
				if (currentStep == null)
					return;
				currentStep.ResetStep ();
				DisplayStep (currentStep);
				return;
			}
		}
		currentTutorial = null;
		HideTutorial();
		throw new UnityException("No registered tutorial step found with name : " + tutorialName);		
	}

	public void HideTutorial ()
	{
		panelTutorial.SetActive(false);
	}

	// buttons behaviors

	public void ButtonSkipOk ()
	{
		HideTutorial();
	}

	public void ButtonPrevButton() 
	{
		currentStep.PrevPage ();
		DisplayStep (currentStep);
	}

	public void ButtonNextButton()
	{
		currentStep.NextPage ();
		DisplayStep (currentStep);
	}

	public void ButtonTutorialToggleOn()
	{
		skipTutorialMode = false;
		panelTutorialPopup.SetActive (false);
		DisplayTutorial ("TutorialStart");

	}

	public void ButtonTutorialToggleOff()
	{
		skipTutorialMode = true;
		panelTutorialPopup.SetActive (false);
	}

	// privates methods

	void Awake ()
	{
		if (instance == null)
			instance = this;
	}

	void Start ()
	{
		panelTutorial.SetActive(false);
		panelTutorialPopup.SetActive (false);
		skipTutorialMode = true;

		if (TimeMaster.instance.isFirstGame) {
			skipTutorialMode = false;
			panelTutorialPopup.SetActive (true);
			foreach (Tutorial tutorial in tutorials)
				tutorial.ResetTutorial ();
		}
	}

	void DisplayStep (TutorialStep step)
	{
		panelTutorial.SetActive (true);
		title.gameObject.SetActive (false);
		explanations.gameObject.SetActive (false);


		// set title image and explanations
		if (! string.IsNullOrEmpty(step.GetCurrentPage ().title)) {
			title.gameObject.SetActive (true);
			title.text = step.GetCurrentPage ().title;
		}
						
		image.sprite = step.GetCurrentPage().sprite;

		if (! string.IsNullOrEmpty(step.GetCurrentPage().explanation)) {
			explanations.gameObject.SetActive (true);
			explanations.text = step.GetCurrentPage().explanation;
		}
	
		RefreshNavigationLayout (step);
	}

	void RefreshNavigationLayout(TutorialStep step) 
	{
		ResetNavigationLayout();

		if (step.pages.Count == 1) {
			okButton.SetActive (true);
		} else {
			InstanciatePageMarkers(step);
			prevButton.SetActive (true);
			prevButton.transform.SetAsFirstSibling ();

			if (step.IsLastPage ()) {
				okButton.SetActive (true);
				okButton.transform.SetAsLastSibling ();
			} else {
				nextButton.SetActive (true);
				nextButton.transform.SetAsLastSibling ();
			}
		}
	}

	void InstanciatePageMarkers (TutorialStep step)
	{
		for (int index = 0; index < step.pages.Count; index++) {
			GameObject marker = null;
			if (index == step.CurrentPageIndex)
				marker = Instantiate(filledDotPrefab) as GameObject;
			else
				marker = Instantiate(emptyDotPrefab) as GameObject;
			marker.transform.SetParent(pagesLayout.transform, false);
		}
	}

	void ResetNavigationLayout ()
	{
		prevButton.SetActive (false);
		nextButton.SetActive (false);
		okButton.SetActive (false);
		foreach (Transform child in pagesLayout.transform) {
			Destroy(child.gameObject);
		}
	}
}
