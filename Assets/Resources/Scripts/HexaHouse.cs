﻿using System.Collections.Generic;
using UnityEngine;

public class HexaHouse : HexaBuilding {

	public HexaHouseType Type;
	
	// Use this for initialization
	void Start () {
		base._type = BuildingType.House;
	}

	// Update is called once per frame
	void Update () {

	}

	public override void OnConstruction(HexCell cell) {
		base.OnConstruction(cell);
		_type = BuildingType.House;
		HouseMaster.instance.RegisterANewHouseOfType(Type);
		TimeMaster.instance.AddBuilding(this);
	}

	public override void OnDestroy() {
		base.OnDestroy();
		if (Type != null)
			HouseMaster.instance.RemoveAHouseOfType(Type);
		TimeMaster.instance.RemoveBuilding(this);
	}
	
}
