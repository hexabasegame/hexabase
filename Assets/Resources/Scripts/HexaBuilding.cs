﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum BuildingType {
	House,
	Factory,
	Warehouse,
	Community,
}

public class HexaBuilding : HexaRenderable {

	public Sprite icon;

	public Direction entraceDirection;

	private HexCell entrace;

	public Resource maintenance;

	public List<Resource> cost = null;

	public List<CellType> canBePlacedOn;
	public List<CellType> mustBePlacedOn;

	public int WorkersCount = 1;
	public HexaWorker workerPrefab;

	public HexaTier requiredTier;

	public AudioClip constructionSound;

	public List<HexaWorker> Workers { get; set; }

	public HexaWorker GetAvailableWorker() {
		foreach (HexaWorker worker in Workers) {
			if (worker.Behavior.currentState == MovingState.WAITING_FOR_PATH) {
				return worker;
			}
		}
		return null;
	}


	public HexCell Entrace {
		get { return entrace; }
		set { entrace = value; }
	}

	public Direction EntraceDirection {
		get { return entraceDirection; }
		set { entraceDirection = value; }
	}
	public HexaBuilding upgrade = null;

	public virtual void OnConstruction(HexCell cell)
	{
		_gridPosition = cell;
		Workers = new List<HexaWorker>();
		for (int i = 0; i < WorkersCount; i++) {
			HexaWorker worker = Instantiate(workerPrefab, GridPosition.position, Quaternion.identity) as HexaWorker;
			worker.Behavior = worker.GetComponent<WorkerBehavior> ();
//			worker.Origin = this;
			Workers.Add(worker);
		}
		Paycost ();
		ShowModel();
		PlayConstructionSound();
	}


    public virtual void OnDestroy()
	{
		DestroyModel ();
	}

    void Paycost(){
		foreach (Resource resource in cost)
			ResourceMaster.instance.RemoveGlobalResourceAmount(resource);
	}

	bool HasEnoughSize(HexCell cell) {
		List<HexCell> extractNeighboorFrom = new List<HexCell> {cell};
		List<HexCell> neighboorToLock = new List<HexCell>();
		List<CellType> foundTypes = new List<CellType>();
		for (int i = 0; i < renderableSize; i++) {
			foreach (HexCell hexCell in extractNeighboorFrom) {
				if (!neighboorToLock.Contains(hexCell)) {
					neighboorToLock.Add(hexCell);
				}
			}
			foreach (HexCell hexCell in neighboorToLock) {
				if (hexCell.MotherCell != null || hexCell.Building != null || !canBePlacedOn.Contains(hexCell.cellType)) {
					return false;
				}
				if (!foundTypes.Contains(hexCell.cellType)) {
					foundTypes.Add(hexCell.cellType);
				}
				extractNeighboorFrom.AddRange(hexCell.Neighboors);
			}
		}
		return mustBePlacedOn.Intersect(foundTypes).Count() == mustBePlacedOn.Count;
	}

	public bool CanBeConstructed() // process consumming
	{
		HexCell cell = RaycastMaster.instance.MouseOverCell;
		if (cell != null && // if mouse is pointing on a tile
		    cell.Building == null && // tile has no renderable on it
		    cell.MotherCell == null && // tile is not in the area of a building
		    ResourceMaster.instance.HasEnoughGlobalResources(GetComponent<HexaBuilding>().cost)) {
			// enough resource to build it
			return HasEnoughSize(cell);
		}
		return false;
	}

	private void PlayConstructionSound() {
		if (constructionSound == null) {
			return;
		}
		AudioSource audioSource = Camera.main.GetComponent<AudioSource>();
		audioSource.clip = constructionSound;
		audioSource.Play();
	}

}
