﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Object = System.Object;
using Random = System.Random;

public static class TerrainSculptor {
//	public static Terrain _terrain;
//	public static float _terrainWidthSize;
//	public static float _terrainHeightSize;
//	public static int _terrainResolution;
//	public static float[,] heights;
	private static readonly Random rand = new Random();
//	public static readonly List<HexCell> river = new List<HexCell>();


	private static readonly Dictionary<Direction, Dictionary<Direction, int>> chances =
		new Dictionary<Direction, Dictionary<Direction, int>> {
			{
				Direction.E,
				new Dictionary<Direction, int> {
					{Direction.E, 40},
					{Direction.NE, 20},
					{Direction.SE, 20},
					{Direction.NW, 10},
					{Direction.SW, 10},
					{Direction.W, 0}
				}
			}, {
				Direction.SE,
				new Dictionary<Direction, int> {
					{Direction.E, 20},
					{Direction.NE, 10},
					{Direction.SE, 40},
					{Direction.NW, 0},
					{Direction.SW, 20},
					{Direction.W, 10}
				}
			}, {
				Direction.SW,
				new Dictionary<Direction, int> {
					{Direction.E, 10},
					{Direction.NE, 0},
					{Direction.SE, 20},
					{Direction.NW, 10},
					{Direction.SW, 40},
					{Direction.W, 20}
				}
			}, {
				Direction.W,
				new Dictionary<Direction, int> {
					{Direction.E, 0},
					{Direction.NE, 10},
					{Direction.SE, 10},
					{Direction.NW, 20},
					{Direction.SW, 20},
					{Direction.W, 40}
				}
			}, {
				Direction.NW,
				new Dictionary<Direction, int> {
					{Direction.E, 10},
					{Direction.NE, 20},
					{Direction.SE, 0},
					{Direction.NW, 40},
					{Direction.SW, 10},
					{Direction.W, 20}
				}
			}, {
				Direction.NE,
				new Dictionary<Direction, int> {
					{Direction.E, 20},
					{Direction.NE, 40},
					{Direction.SE, 10},
					{Direction.NW, 20},
					{Direction.SW, 0},
					{Direction.W, 10}
				}
			}
		};

	public static void ThreadedRiverCreation()
	{
		List<Thread> threads = new List<Thread>();
		for (int i = 0; i < 3; i++)
		{
			Thread thread = new Thread(SmoothTerrain);
			threads.Add(thread);
			thread.Start();
			thread.Join();
		}
	}

	
	public static void DoYourThing() {
//		Debug.Log("Creating Terrain Data");
		FlattenTerrain();
		for (int i = 0; i < 10; i++) {
			CreateMountainInTerrain(rand.Next(0, (HexMetrics.height + HexMetrics.width) / 2));
		}
		for (int i = 0; i < 10; i++) {
			CreateRiverInTerrain(rand.Next(0, (HexMetrics.height + HexMetrics.width) / 2));
		}
//		BuildMountainAroundTerrain();
		BuildRiverAroundTerrain();
		for (int i = 0; i < 3; i++)
			SmoothTerrain();
		HexaTerrainData.Instance.ActiveTerrain.terrainData.SetHeights(0, 0, HexaTerrainData.Instance.Heights);
	}

	public static void DoYourThingOnLoad(float[,] terrainHeights, List<int> riverIndexes, List<int> mountainTopIndexes, List<int> mountainSideIndexes, List<int> mountainFootIndexes)
	
	{
//		Debug.Log("Loading Terrain Data");
		HexaTerrainData.Instance.ResetTerrain();
//		
//		FlattenTerrain();
//		TerrainData.Instance.River = new List<HexCell>();
		foreach (int i in riverIndexes)
		{
			HexCell cell = HexGrid.GetCells()[i];
			cell.cellType = CellType.River;
			cell.originalType = CellType.River;
			HexaTerrainData.Instance.River.Add(cell);
			HexaTerrainData.Instance.RemoveTreeFromCell(cell);
		}
		foreach (int i in mountainTopIndexes)
		{
			HexCell cell = HexGrid.GetCells()[i];
			cell.cellType = CellType.Mountain;
			cell.originalType = CellType.Mountain;
			HexaTerrainData.Instance.Mountain.Add(cell);
			HexaTerrainData.Instance.RemoveTreeFromCell(cell);
		}
		foreach (int i in mountainSideIndexes)
		{
			HexCell cell = HexGrid.GetCells()[i];
			cell.cellType = CellType.MountainSide;
			cell.originalType = CellType.MountainSide;
			HexaTerrainData.Instance.MountainSide.Add(cell);
			HexaTerrainData.Instance.RemoveTreeFromCell(cell);
		}
		foreach (int i in mountainFootIndexes)
		{
			HexCell cell = HexGrid.GetCells()[i];
			cell.cellType = CellType.MountainFeet;
			cell.originalType = CellType.MountainFeet;
			HexaTerrainData.Instance.MountainFeet.Add(cell);
			HexaTerrainData.Instance.RemoveTreeFromCell(cell);
		}
		Debug.Log(String.Format("There is {0} River on map", HexaTerrainData.Instance.River.Count));
		Debug.Log(String.Format("There is {0} Mountain on map", HexaTerrainData.Instance.Mountain.Count));
		Debug.Log(String.Format("There is {0} MountainSide on map", HexaTerrainData.Instance.MountainSide.Count));
		Debug.Log(String.Format("There is {0} MountainFeet on map", HexaTerrainData.Instance.MountainFeet.Count));
		HexaTerrainData.Instance.Heights = terrainHeights;
		HexaTerrainData.Instance.ActiveTerrain.terrainData.SetHeights(0, 0, HexaTerrainData.Instance.Heights);
	}

	private static void FlattenTerrain() {
		float[,] heights = HexaTerrainData.Instance.Heights;
		for (int i = 0; i < HexaTerrainData.Instance.TerrainResolution; i++) {
			for (int j = 0; j < HexaTerrainData.Instance.TerrainResolution; j++) {
				heights[i, j] = HexaTerrainData.GroundLevel;
			}
		}
		HexaTerrainData.Instance.Heights = heights;
	}

	private static void SmoothTerrain() {
		float[,] heights = HexaTerrainData.Instance.Heights;
		int terrainResolution = HexaTerrainData.Instance.TerrainResolution;
		float averageY;
		for (int x = 1; x < terrainResolution - 1; x++) {
			for (int y =  1; y < terrainResolution - 1; y++) {
				averageY = heights[x + 0, y - 0];

				//LineAbove
				averageY += heights[x + 1, y - 1];
				averageY += heights[x + 1, y + 0];
				averageY += heights[x + 1, y + 1];

				//SameLine
				averageY += heights[x + 0, y - 1];
				averageY += heights[x + 0, y + 1];

				//LineBelow
				averageY += heights[x - 1, y - 1];
				averageY += heights[x - 1, y + 0];
				averageY += heights[x - 1, y + 1];

				heights[x, y] = averageY / 9f;
			}
		}
		HexaTerrainData.Instance.Heights = heights;
	}

	private static void CreateRiverInTerrain(int lenght) {
		HexCell cell = HexGrid.TwoDCells[rand.Next(0, HexMetrics.width)][rand.Next(0, HexMetrics.height)];
		Direction d = GetFirstDirectionRandomly(cell);
		for (int i = 0; i < lenght; i++) {
			cell = cell.GetNeighboorByDirection(GetNextRandomDirection(cell, d));
			SetCellAsTypeAndAddToListAndSetElevation(cell, CellType.River, HexaTerrainData.Instance.River, HexaTerrainData.RiverBedLevel, 2);
		}
	}

	private static void CreateMountainInTerrain(int lenght) {
		HexCell cell = HexGrid.TwoDCells[rand.Next(0, HexMetrics.width)][rand.Next(0, HexMetrics.height)];
		Direction d = GetFirstDirectionRandomly(cell);

		for (int i = 0; i < lenght; i++) {
			cell = cell.GetNeighboorByDirection(GetNextRandomDirection(cell, d));
			SetCellAsTypeAndAddToListAndSetElevation(cell, CellType.Mountain, HexaTerrainData.Instance.Mountain, HexaTerrainData.MountainTopLevel, 4);
			foreach (HexCell neighboor in cell.Neighboors) {
				if (HexaTerrainData.Instance.Mountain.Contains(neighboor)) continue;
				SetCellAsTypeAndAddToListAndSetElevation(neighboor, CellType.MountainSide, HexaTerrainData.Instance.MountainSide, HexaTerrainData.MountainTopLevel / 3 * 2, 3);
			}
		}

		foreach (HexCell mountain in HexaTerrainData.Instance.MountainSide) {
			foreach (HexCell neighboor in mountain.Neighboors) {
				if (HexaTerrainData.Instance.Mountain.Contains(neighboor) 
				    || HexaTerrainData.Instance.MountainSide.Contains(neighboor)
				    || HexaTerrainData.Instance.MountainFeet.Contains(neighboor)) continue;
				SetCellAsTypeAndAddToListAndSetElevation(neighboor, CellType.MountainFeet, HexaTerrainData.Instance.MountainFeet, HexaTerrainData.MountainTopLevel / 3, 2);
			}
		}
	}

	private static void BuildRiverAroundTerrain()
	{
		HexCell cell;
		for (int i = 0; i < HexMetrics.width; i++)
		{
			for (int j = 0; j < HexMetrics.height; j++)
			{
				if (i != 0 && i != HexMetrics.width - 1 && j != 0 && j != HexMetrics.height - 1) continue;
				cell = HexGrid.TwoDCells[i][j];
				SetCellAsTypeAndAddToListAndSetElevation(cell, CellType.River, HexaTerrainData.Instance.River, HexaTerrainData.RiverBedLevel, 0);
			}
		}
		for (int i2 = 0; i2 < HexaTerrainData.Instance.TerrainResolution; i2++) {
			for (int j2 = 0; j2 < HexaTerrainData.Instance.TerrainResolution; j2++) {
				if (i2 > 20 && i2 <  HexaTerrainData.Instance.TerrainResolution - 20 && j2 > 20 && j2 <  HexaTerrainData.Instance.TerrainResolution - 20) continue;
				HexaTerrainData.Instance.Heights[i2, j2] = HexaTerrainData.RiverBedLevel;
			}
		}
	}

	private static void BuildMountainAroundTerrain() {
		HexCell cell;
		List<HexCell> mountainSides = new List<HexCell>();
		for (int i = 2; i < HexMetrics.width - 2; i++) {
			for (int j = 2; j < HexMetrics.height - 2; j++) {
				if (i > 3 && i < HexMetrics.width - 4 && j > 3 && j < HexMetrics.height - 4) continue;
				cell = HexGrid.TwoDCells[i][j];
				SetCellAsTypeAndAddToListAndSetElevation(cell, CellType.Mountain, HexaTerrainData.Instance.Mountain, HexaTerrainData.MountainTopLevel, 4);
				foreach (HexCell neighboor in cell.Neighboors) {
					if (HexaTerrainData.Instance.Mountain.Contains(neighboor)) continue;
					SetCellAsTypeAndAddToListAndSetElevation(neighboor, CellType.MountainSide, HexaTerrainData.Instance.MountainSide, HexaTerrainData.MountainTopLevel / 3 * 2, 3);
					mountainSides.Add(neighboor);
				}
			}
		}
		foreach (HexCell mountain in mountainSides) {
			foreach (HexCell neighboor in mountain.Neighboors) {
				if (HexaTerrainData.Instance.Mountain.Contains(neighboor) 
				    || HexaTerrainData.Instance.MountainSide.Contains(neighboor)
				    || HexaTerrainData.Instance.MountainFeet.Contains(neighboor)) continue;
				SetCellAsTypeAndAddToListAndSetElevation(neighboor, CellType.MountainFeet, HexaTerrainData.Instance.MountainFeet, HexaTerrainData.MountainTopLevel / 3, 2);
			}
		}
	}

	private static void SetCellAsTypeAndAddToListAndSetElevation(HexCell cell, CellType type, List<HexCell> list, float elevation, int farFromBorder) {
		if (cell.coordinates.BaseX < farFromBorder || cell.coordinates.BaseX > HexMetrics.width - farFromBorder || cell.coordinates.BaseY < farFromBorder ||
			cell.coordinates.BaseY > HexMetrics.height - farFromBorder)
			return;
		cell.cellType = type;
		cell.originalType = type;
		list.Add(cell);
		HexaTerrainData.Instance.RemoveTreeFromCell(cell);
		SetTerrainElevationOnCell(cell, elevation);
	}
	
	public static void SetTerrainElevationOnCell(HexCell cell, float height) {
		int terrainMinHeightCell = Mathf.RoundToInt(((cell.position.z+HexMetrics.outerRadius)-HexMetrics.outerRadius) / HexaTerrainData.Instance.TerrainHeightSize);
		int terrainMaxHeightCell = Mathf.RoundToInt(((cell.position.z+HexMetrics.outerRadius)+HexMetrics.outerRadius) / HexaTerrainData.Instance.TerrainHeightSize);

		int terrainMinWidthCell = Mathf.RoundToInt(((cell.position.x+HexMetrics.innerRadius)-HexMetrics.innerRadius) / HexaTerrainData.Instance.TerrainWidthSize);
		int terrainMaxWidthCell = Mathf.RoundToInt(((cell.position.x+HexMetrics.innerRadius)+HexMetrics.innerRadius) / HexaTerrainData.Instance.TerrainWidthSize);
		
		for (int i = Math.Max(terrainMinHeightCell, 0); i < terrainMaxHeightCell; i++) {
			for (int j = Math.Max(terrainMinWidthCell, 0); j < terrainMaxWidthCell; j++) {
				HexaTerrainData.Instance.Heights[i, j] = height;
			}
		}
	}

	public static void SetSingleCellElevationOnTerrain(HexCell cell, float height) {
		int terrainMinHeightCell = Mathf.RoundToInt(((cell.position.z+HexMetrics.outerRadius)-HexMetrics.outerRadius) / HexaTerrainData.Instance.TerrainHeightSize);
		int terrainMaxHeightCell = Mathf.RoundToInt(((cell.position.z+HexMetrics.outerRadius)+HexMetrics.outerRadius) / HexaTerrainData.Instance.TerrainHeightSize);

		int terrainMinWidthCell = Mathf.RoundToInt(((cell.position.x+HexMetrics.innerRadius)-HexMetrics.innerRadius) / HexaTerrainData.Instance.TerrainWidthSize);
		int terrainMaxWidthCell = Mathf.RoundToInt(((cell.position.x+HexMetrics.innerRadius)+HexMetrics.innerRadius) / HexaTerrainData.Instance.TerrainWidthSize);
		
		float[,] transform = new float[terrainMaxWidthCell-terrainMinWidthCell, terrainMaxHeightCell-terrainMinHeightCell];
		int cnt1 = 0;
		for (int i = Math.Max(terrainMinHeightCell, 0); i < terrainMaxHeightCell; i++) {
			int cnt2 = 0;
			for (int j = Math.Max(terrainMinWidthCell, 0); j < terrainMaxWidthCell; j++) {
				transform[cnt2, cnt1] = height;
				cnt2++;
			}
			cnt1++;
		}
		HexaTerrainData.Instance.ActiveTerrain.terrainData.SetHeights(terrainMinWidthCell, terrainMinHeightCell, transform);
		HexaTerrainData.Instance.Heights = HexaTerrainData.Instance.ActiveTerrain.terrainData.GetHeights(0, 0, HexaTerrainData.Instance.TerrainResolution, HexaTerrainData.Instance.TerrainResolution);
	}
	
	private static Direction GetFirstDirectionRandomly(HexCell cell) {
		while (true) {
			int tmp = rand.Next(6);
			List<Direction> ds = new List<Direction>{Direction.E, Direction.NE, Direction.NW, Direction.SE, Direction.SW, Direction.W};
			try {
				cell.GetNeighboorByDirection(ds[tmp]);
				return ds[tmp];
			}
			catch (OnTheBorderException) {}
		}
	}

	private static Direction GetNextRandomDirection(HexCell cell, Direction direction) {
		while (true) {
			foreach (KeyValuePair<Direction, int> keyValuePair in chances[direction]) {
				int tmp = rand.Next(100);
				if (tmp < keyValuePair.Value) {
					try {
						cell.GetNeighboorByDirection(keyValuePair.Key);
						return keyValuePair.Key;
					}
					catch (OnTheBorderException) {}
				}
			}
		}
	}
}
