﻿public class HexaWorker : HexaRenderable {


	public Resource _carry = null;

	private WorkerBehavior _behavior;
    private int _maxCapacity = 3;
	private HexaBuilding _destination = null;
	private HexaBuilding _origin = null;

	public HexaBuilding Origin {
		get {return _origin;}
		set {_origin = value;}
	}

	public Resource Carry {
		get { return _carry; }
		set { _carry = value; }
	}

	public WorkerBehavior Behavior {
		get { return _behavior; }
	    set {
	        _behavior = value;
	        _behavior._worker = this;
	    }
	}

	public HexaBuilding Destination {
		get { return _destination; }
		set { _destination = value; }
	}

    public int MaxCapacity {
        get { return _maxCapacity; }
        set { _maxCapacity = value; }
    }

	void Start(){
		ShowModel ();
	}

	
	public bool IsCarryingResource()
	{
		return (Carry != null && Carry.amount > 0);
	}
}
