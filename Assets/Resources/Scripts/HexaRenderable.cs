﻿using UnityEngine;

public class HexaRenderable : MonoBehaviour {

	public GameObject modelPrefab = null;
    public new string name;
	public int renderableSize = 1;
	[HideInInspector] public bool isSelected = false;
	[HideInInspector] public float modelHeight = 0f;

	GameObject model = null;
	protected HexCell _gridPosition;
	public BuildingType _type;

	public BuildingType Type {
		get { return _type; }
	}

	public HexCell GridPosition {
		get { return _gridPosition; }
	}

	public void ShowModel ()
	{
		if (!Model) {
			Model = Instantiate (modelPrefab) as GameObject;
			Model.transform.SetParent(this.transform);
			Model.transform.position = this.transform.position;
			Model.transform.rotation = modelPrefab.transform.rotation;
			foreach(Transform child in Model.transform){
				MeshRenderer renderer = child.GetComponent<MeshRenderer>();
				if (renderer == null)
					continue;
				float height = renderer.bounds.extents.y;
				if (modelHeight < height)
					modelHeight = height;
			}
			if (BuildMaster.instance.DragAndDropBuilding != null) {
				Model.transform.localRotation = BuildMaster.instance.DragAndDropBuilding.transform.localRotation;
			}
		} else
			Model.SetActive(true);
	}

	public void HideModel ()
	{
		if (Model)
			Model.SetActive(false);
	}

	public void DestroyModel()
	{
		if (Model)
			Destroy (Model);
	}

	public GameObject Model {
		get {
			return model;
		}
		set {
			model = value;
		}
	}

	void OnDestroy(){
		DestroyModel ();
	}

	public HexCell CheckForEntranceOpeningOrChange(Direction originalDirection, HexCell originalEntrance, int tryCount) {
		Direction newDirection = originalDirection;
		HexCell newEntrance = originalEntrance;
		foreach (HexCell neighboor in newEntrance.Neighboors) {
			if (neighboor.cellType == CellType.Grass || neighboor.cellType == CellType.Road) {
				return originalEntrance;
			}
		}
		tryCount++;
		return FindEntranceByDirection(DirectionMethods.GetNext(originalDirection), tryCount);
	}

	public HexCell FindEntranceByDirection(Direction d) {
		return FindEntranceByDirection(d, 0);
	}


	public HexCell FindEntranceByDirection(Direction d, int tryCount) {
		HexCell entrance = _gridPosition;
		for (int i = 0; i < renderableSize -1; i++) {
			entrance = entrance.GetNeighboorByDirection(d);
		}
		if (tryCount == 7) {
			return entrance;
		}
		return CheckForEntranceOpeningOrChange(d, entrance, tryCount);
	}
}
