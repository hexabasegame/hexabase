﻿using System;

[Serializable]
public class TierRequirements
{
    public HexaBuilding building;
    public int amount;

    public TierRequirements() {}

    public TierRequirements(HexaBuilding building, int amount)
    {
        this.building = building;
        this.amount = amount;
    }

}