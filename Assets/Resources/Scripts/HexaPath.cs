﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class HexaPath {
	public static readonly int BuildingWeight = 1500;
	public static readonly int unalkableWeight = 1300;
	public static readonly int MotherCellWeight = 750;
	public static readonly int EndingWeight = 3000;

	private static GameObject[] hexaWeighTextList = new GameObject[HexMetrics.width * HexMetrics.height];
	private static GameObject hexaWeighText = Resources.Load<GameObject>("GroundTextCanvas");

	public static Dictionary<HexCell, int> GetWeightedNeighbours(HexCell start, HexCell end) {
		List<HexCell> toWeight = new List<HexCell>();
		Dictionary<HexCell, int> weightedNeighbours = new Dictionary<HexCell, int>();
		weightedNeighbours[start] = 0;
		toWeight.Add(start);

		int i = 0;
		while (toWeight.Count > 0) {
			HexCell current = toWeight[0];
			toWeight.RemoveAt(0);
			foreach (HexCell nextNeightboor in current.Neighboors) {
				int prevWeight = EndingWeight;
				if (weightedNeighbours.ContainsKey(nextNeightboor)) {
					prevWeight = weightedNeighbours[nextNeightboor];
				}
				switch (nextNeightboor.cellType) {
					case CellType.Road:
					case CellType.Grass:
					case CellType.Beach:
						if (weightedNeighbours[current] + 1 < prevWeight) {
							weightedNeighbours[nextNeightboor] = Math.Min(weightedNeighbours[current] + 1, prevWeight);
							toWeight.Add(nextNeightboor);
						}
						break;
					case CellType.River:
					case CellType.MountainSide:
					case CellType.Mountain:
						weightedNeighbours[nextNeightboor] = unalkableWeight;
						break;
					case CellType.BuildingCenter:
						weightedNeighbours[nextNeightboor] = BuildingWeight;
						break;
					case CellType.BuildingSide:
						if (weightedNeighbours[current] < prevWeight) {
							weightedNeighbours[nextNeightboor] = Math.Min(weightedNeighbours[current] + 4, prevWeight);
							toWeight.Add(nextNeightboor);
						}
						break;
				}
//				DebugShowPathFindingValue(weightedNeighbours, nextNeightboor);
				if (nextNeightboor == end) {
					return weightedNeighbours;
				}
			}
			if (current == end) {
				return weightedNeighbours;
			}
		}
		return null;
	}

	private static void DebugShowPathFindingValue(Dictionary<HexCell, int> weightedNeighbours, HexCell nextNeightboor) {
		int pos = Array.IndexOf(HexGrid.GetCells(), nextNeightboor);
		if (hexaWeighTextList[pos] == null) {
			hexaWeighTextList[pos] = GameObject.Instantiate(hexaWeighText, nextNeightboor.position,
				hexaWeighText.transform.rotation);
			hexaWeighTextList[pos].transform.position = new Vector3(
				hexaWeighTextList[pos].transform.position.x,
				hexaWeighTextList[pos].transform.position.y + 1,
				hexaWeighTextList[pos].transform.position.z
			);
			hexaWeighTextList[pos].GetComponent<Text>().text = weightedNeighbours[nextNeightboor].ToString();
		}
		else {
			hexaWeighTextList[pos].GetComponent<Text>().text = weightedNeighbours[nextNeightboor].ToString();
		}
	}


	public static List<HexCell> GetLowestWeightedCell(Dictionary<HexCell, int> weightedNeighbours, HexCell current,
		List<HexCell> list) {
		List<HexCell> ligthests = new List<HexCell> {current};
		foreach (var cell in list) {
			if (ligthests.Count == 0) {
				ligthests.Add(cell);
			}
			try {
				if (weightedNeighbours[ligthests[0]] > weightedNeighbours[cell]) {
					ligthests = new List<HexCell> {cell};
				}
				else if (weightedNeighbours[ligthests[0]] == weightedNeighbours[cell]) {
					ligthests.Add(cell);
				}
			}
			catch (KeyNotFoundException) {
//				Debug.Log("Lighest is: "+ligthest.coordinates.ToString());
//				Debug.Log("cell is: "+cell.coordinates.ToString());
			}
		}
		return ligthests;
	}

	public static List<HexCell> GetPathFromBuildingToBuilding(HexCell origin, HexCell destination) {
		HexCell originEntrance = origin.Building.FindEntranceByDirection(DirectionCellToCell(origin, destination));
		HexCell destinationEntrance = destination.Building.FindEntranceByDirection(DirectionCellToCell(destination, origin));
		return GetPathFromXtoY(originEntrance, destinationEntrance);
	}

	public static List<HexCell> GetPathFromXtoY(HexCell x, HexCell y) {
		var rand = new Random();
		Dictionary<HexCell, int> weightedNeighbours = GetWeightedNeighbours(x, y);
		if (weightedNeighbours == null) {
			return new List<HexCell>();
		}
		weightedNeighbours[x] = 0;
		weightedNeighbours[y] = EndingWeight;
		List<HexCell> path = new List<HexCell>();
		HexCell current = y;
		path.Add(current);
		while (current != x) {
			List<HexCell> results = GetLowestWeightedCell(weightedNeighbours, current, current.Neighboors);
			int index = rand.Next(0, results.Count);
			current = results[index];
			path.Add(current);
		}
		path.Reverse();
		return path;
	}

	/*****************
	 **UTILITY TOOLS**
	 *****************/

	public static long CubeDistance(HexCell a, HexCell b) {
		return Math.Max(Math.Abs(a.coordinates.X - b.coordinates.X),
			Math.Max(Math.Abs(a.coordinates.Y - b.coordinates.Y), Math.Abs(a.coordinates.Z - b.coordinates.Z)));
	}

	public static Direction DirectionCellToCell(HexCell a, HexCell b) {
//		Debug.Log("a = " + a.coordinates.BaseX + "|" + a.coordinates.BaseY);
//		Debug.Log("b = " + b.coordinates.BaseX + "|" + b.coordinates.BaseY);
		string direction = "";
		if (b.coordinates.BaseY - a.coordinates.BaseY > 0) {
			direction += "N";
		}
		else if (b.coordinates.BaseY - a.coordinates.BaseY < 0) {
			direction += "S";
		}

		if (b.coordinates.BaseX - a.coordinates.BaseX > 0) {
			direction += "E";
		}
		else if (b.coordinates.BaseX - a.coordinates.BaseX < 0) {
			direction += "W";
		}
		else {
			if (a.coordinates.BaseY % 2 == 0) {
				direction += "E";
			}
			else {
				direction += "W";
			}
		}
//		Debug.Log("Direction: " + direction);
		return DirectionMethods.FromString(direction);
	}
}
