﻿using System.Collections.Generic;
using UnityEngine;

public enum MovingState{
	WAITING_FOR_PATH,
	START,
	ON_WAY_TO_DESTINATION,
	AT_DESTINATION,
	PAUSED,
	FORCE_RETURN,
	RESET,
	ERROR,
	NONE
}

public class MoveBehavior : MonoBehaviour {

	public MovingState currentState = MovingState.WAITING_FOR_PATH;
    public int currentCellIndex = 0;
    public float speed = 5F;
	public bool comeBack = true;

	protected List<HexCell> path = null;
	protected HexCell startCell = null;
	protected HexCell destinationCell = null;

	private Vector3 target;
	private Transform current;
	private MovingState saveState = MovingState.NONE;
	private bool pathHasToReverse = true;


	// PUBLIC //


	public void SetNewPath(List<HexCell> newPath)
	{
		if (currentState != MovingState.WAITING_FOR_PATH || newPath == null || newPath.Count == 0)
			return;

		foreach (HexCell hexCell in newPath) {
			TerrainTexturer.TexturizeCellWithUniqueTexture(hexCell, TextureEnum.Road);
		}
		currentCellIndex = 0;
		path = newPath;
		startCell = path [0];
		destinationCell = path [path.Count - 1];
		pathHasToReverse = comeBack;
		Update ();
	}

	public void ForceReturn()
	{
		if (!pathHasToReverse)
			return;
		currentState = MovingState.FORCE_RETURN;
		Update ();
	}

	public void Reset()
	{
		currentState = MovingState.RESET;
		Update ();
	}

	public void Pause()
	{
		if (currentState != MovingState.PAUSED) {
			saveState = currentState;
			currentState = MovingState.PAUSED;
			OnPauseBegin (path [currentCellIndex]); // callback;
			Update ();

		} else {
			currentState = saveState;
			saveState = MovingState.NONE;
			OnPauseEnd (path [currentCellIndex]);
			Update ();
		}
	}


	// CALLBACKS //

	protected virtual void OnCreation () {}

	protected virtual void OnStart(HexCell cell) {}

	protected virtual void OnEnterCell(HexCell cell) {}

	protected virtual void OnWalk(HexCell cell) {}

	protected virtual void OnDestination(HexCell cell) {}

	protected virtual void OnEnd(HexCell cell) {}

	protected virtual void OnPauseBegin(HexCell cell) {}

	protected virtual void OnPauseEnd(HexCell cell) {}

	protected virtual void OnForceReturn(HexCell cell) {}

	protected virtual void BeforeReset(HexCell cell) {}


	// PRIVATE //

	void Start () {
		OnCreation();  // callback
	}

	void Update () {
		currentState = StateMachine (currentState);
	}

	void ResetPath ()
	{
		path = null;
		currentCellIndex = 0;
		pathHasToReverse = false;
	}

	void ReversePath()
	{
		path.Reverse();
		currentCellIndex = Mathf.Abs((path.Count-1) - currentCellIndex );
		destinationCell = path [path.Count - 1];
		pathHasToReverse = false;
	}

	bool HasEndThePath()
	{
		return currentCellIndex == path.Count - 1;
	}

	void Walk()
	{
		current = this.transform;
		target = path[currentCellIndex].position;
		if (target != current.position) {
			MoveTowardNextTile ();

		} else {
			SetNextTile ();
		}
	}

	void SetNextTile()
	{
		OnEnterCell (path[currentCellIndex]); // callback
		currentCellIndex++;
		target = path[currentCellIndex].position;

	}

	void MoveTowardNextTile()
	{
		float step = speed * Time.deltaTime;
		current.position = Vector3.MoveTowards(current.position, target, step);
		OnWalk(path[currentCellIndex]); // callback
		current.LookAt(target);
	}

	// STATE MACHINE //

	MovingState StateMachine(MovingState currentState)
	{
		if (path == null || path.Count == 0) {
			return WaitingForPathState();
		}

		switch (currentState)
		{
		case MovingState.FORCE_RETURN:
			return ForceReturnState ();
		case MovingState.RESET:
			return ResetState ();
		case MovingState.PAUSED:
			return PausedState ();
		case MovingState.WAITING_FOR_PATH:
			return WaitingForPathState();
		case MovingState.START:
			return StartState ();
		case MovingState.ON_WAY_TO_DESTINATION:
			return OnWayToDestinationState ();
		case MovingState.AT_DESTINATION:
			return AtDestinationState ();
		default:
			return MovingState.ERROR;
		}
	}

	// STATES //

	MovingState ResetState()
	{
		BeforeReset (path[currentCellIndex]); // callback
		ResetPath ();
		return MovingState.WAITING_FOR_PATH;
	}

	MovingState ForceReturnState()
	{
		ReversePath ();
		OnForceReturn (path[currentCellIndex]); // callback
		return MovingState.START;
	}

	MovingState WaitingForPathState()
	{
		if (path == null) {
			ResetPath ();
			return MovingState.WAITING_FOR_PATH;
		}
		return MovingState.START;
	}

	MovingState StartState()
	{
		if (path == null || path.Count == 0) {
			return MovingState.WAITING_FOR_PATH;
		}
		OnStart(path[0]);  // callback
		return MovingState.ON_WAY_TO_DESTINATION;
	}

	MovingState OnWayToDestinationState()
	{
		if (HasEndThePath() && target == current.position) {
			return MovingState.AT_DESTINATION;
		}
		Walk ();
		return MovingState.ON_WAY_TO_DESTINATION;
	}

	MovingState AtDestinationState()
	{
		OnDestination (path[currentCellIndex]); // callback
		if (pathHasToReverse){
			ReversePath();
			return MovingState.START;
		} else {
			path = null;
			return MovingState.WAITING_FOR_PATH;
		}
	}

	MovingState PausedState()
	{
		return MovingState.PAUSED;
	}
}
