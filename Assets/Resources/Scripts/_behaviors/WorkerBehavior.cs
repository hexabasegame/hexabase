﻿using UnityEngine;

public class WorkerBehavior : MoveBehavior {

	public HexaWorker _worker;
	public HexaRenderable destination;
	
	Animator animator = null;

	// PUBLIC //

	public void Abort()
	{
		Reset ();
		transform.position = startCell.position;
		FillWarehouse ();
	}

	// PRIVATE //

	protected override void OnCreation() 
	{
		Hide();
	}

	protected override void OnStart(HexCell cell)
	{
		UnHide ();
		animator = _worker.Model.GetComponent<Animator>();
		animator.SetBool("is_walking", true);
		if (path == null || path.Count == 0) {
			return;
		}

		if (DestinationStillExists() == false)
			return;
		_worker.Model.SetActive(true);
		destination = path[path.Count - 1].MotherCell.Building;
	}

	protected override void OnEnterCell(HexCell cell)
	{
		if (DestinationStillExists() == false)
			return;
		animator.SetBool("is_walking", true);
		if (_worker.Origin && _worker.Origin.isSelected)
			UIMaster.instance.panelDescription.RefreshWarehouseDisplay ();
	}

	protected override void OnDestination(HexCell cell) {
		if (DestinationStillExists() == false)
			return;
		animator.SetBool("is_walking", false);
		if (cell.MotherCell.HasBuilding)
		{
			if (_worker.Origin.Type == BuildingType.Warehouse) {
				if (cell.MotherCell.Building.Type == BuildingType.Factory) {
					FillFactory();
					TakeFactoryOutput();
				} else if (cell.MotherCell.Building.Type == BuildingType.Warehouse) {
					FillWarehouse();
				}
			} else if (_worker.Origin.Type == BuildingType.Factory) {
				FillFactory();
			} else {
				Hide ();
			}

//			if (cell.MotherCell.Building is HexaFactory) {
//				FillFactory ();
//			}
//			if (cell.MotherCell.Building is HexaWareHouse) {
//				FillWarehouse();
//			}

		}
	}

	protected override void OnWalk(HexCell cell)
	{
		_worker.Model.transform.position = transform.position;
	}

	void FillFactory()
	{
		if (_worker.Carry == null)
			return;
		if (_worker.Destination.Type == BuildingType.Factory)
			((HexaFactory)_worker.Destination).DropInputResource (_worker.Carry, _worker.Carry.amount);
		_worker.Carry = null;
	}

	void TakeFactoryOutput()
	{
		if (_worker.Destination.Type == BuildingType.Factory)
			_worker.Carry = ((HexaFactory)_worker.Destination).TakeMostProducedOutputResource (_worker.MaxCapacity);
	}

	void FillWarehouse()
	{
		if (_worker.Carry != null && _worker.Carry.amount > 0)
			ResourceMaster.instance.AddGlobalResourceAmount (_worker.Carry);
		_worker.Carry = null;
		Hide ();
	}

	void Hide()
	{
		_worker.Model.SetActive(false);
	}

	void UnHide ()
	{
		_worker.Model.SetActive(true);
	}

	bool DestinationStillExists ()
	{
		if (destinationCell.MotherCell == null || !destinationCell.MotherCell.HasBuilding) {
			ForceReturn ();
			return false;
		}
		return true;
	}
}
