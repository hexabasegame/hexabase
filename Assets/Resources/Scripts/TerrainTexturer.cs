﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TextureEnum {
	public static int Grass=0;
	public static int River=0;
	public static int Beach=1;
	public static int MountainFeet=2;
	public static int MountainSide=2;
	public static int Mountain=3;
	public static int BuildingCenter=0;
	public static int BuildingSide=0;
	public static int Road=4;

	public static int GetValuesLenght() {
		return 5;
	}
}

public static class TerrainTexturer {
	private static Terrain _terrain;
	private static UnityEngine.TerrainData terrainData;
	private static int alphamapHeight;
	private static int alphamapWidth;
	private static float[,,] splatmapData;
	private static float _terrainAlphaWidthSize;
	private static float _terrainAlphaHeightSize;

	private static void SetBaseData() {
		terrainData = HexaTerrainData.Instance.ActiveTerrain.terrainData;

		alphamapHeight = HexaTerrainData.Instance.ActiveTerrain.terrainData.alphamapHeight;
		alphamapWidth = HexaTerrainData.Instance.ActiveTerrain.terrainData.alphamapWidth;

		_terrainAlphaWidthSize = terrainData.size.x / alphamapWidth;
		_terrainAlphaHeightSize = terrainData.size.z / alphamapHeight;

		splatmapData = new float[alphamapWidth, alphamapHeight, terrainData.alphamapLayers];

	}

	public static void DoYourThing() {
		HexaTerrainData.Instance.ResetTerrain();
		SetBaseData();
		SetBaseTexture();

		float[] mapValue;
		
		mapValue = new[]{0, 1f, 0, 0, 0};
		List<HexCell> beachs = GetAllBeaches();
		foreach (HexCell beach in beachs) {
			TexturizeCell(beach, mapValue);
		}

		mapValue = new []{0,0,1f,0, 0};
		foreach (HexCell mountainFoot in HexaTerrainData.Instance.MountainFeet) {
			TexturizeCell(mountainFoot, mapValue);
		}

		mapValue = new []{0,0,0.5f, 0.5f, 0};
		foreach (HexCell mountainSide in HexaTerrainData.Instance.MountainSide) {
			TexturizeCell(mountainSide, mapValue);
		}
		
		mapValue = new []{0,0,0, 1f, 0};
		foreach (HexCell mountain in HexaTerrainData.Instance.Mountain) {
			TexturizeCell(mountain, mapValue);
		}
		
		mapValue = new []{0,0,0, 0, 1f};
		foreach (HexCell road in HexaTerrainData.Instance.Roads) {
			TexturizeCell(road, mapValue);
		}
		

		// Finally assign the new splatmap to the terrainData:
		terrainData.SetAlphamaps(0, 0, splatmapData);
	}

	public static void ReturnCellToOriginalTexture(HexCell cell) {
		float[] textureValues = new float[TextureEnum.GetValuesLenght()];
		Debug.Log(cell.originalType.ToString());
	}
	
	public static void TexturizeCellWithUniqueTexture(HexCell cell, int texture) {
		float[] textureValues = new float[TextureEnum.GetValuesLenght()];
		for (int i = 0; i < textureValues.Length; i++) {
			textureValues[i] = 0;
			if (texture == i) {
				textureValues[i] = 1f;
			}

			if (i == TextureEnum.Road) {
				HexaTerrainData.Instance.Roads.Add(cell);
				HexaTerrainData.Instance.RemoveTreeFromCell(cell);
			}
		}
		TexturizeCell(cell, textureValues);
		terrainData.SetAlphamaps(0, 0, splatmapData);
	}
	
	private static void TexturizeCell(HexCell cell, float[] textureValues) {
		int terrainMinHeightCell = Mathf.RoundToInt(((cell.position.z+HexMetrics.outerRadius)-HexMetrics.outerRadius) / _terrainAlphaHeightSize);
		int terrainMaxHeightCell = Mathf.RoundToInt(((cell.position.z+HexMetrics.outerRadius)+HexMetrics.outerRadius) / _terrainAlphaHeightSize);
		int terrainMinWidthCell = Mathf.RoundToInt(((cell.position.x+HexMetrics.innerRadius)-HexMetrics.innerRadius) / _terrainAlphaWidthSize);
		int terrainMaxWidthCell = Mathf.RoundToInt(((cell.position.x+HexMetrics.innerRadius)+HexMetrics.innerRadius) / _terrainAlphaWidthSize);

		for (int i = Math.Max(terrainMinHeightCell, 0); i < terrainMaxHeightCell; i++) {
			for (int j = Math.Max(terrainMinWidthCell, 0); j < terrainMaxWidthCell; j++) {
				for (int v = 0; v < textureValues.Length; v++) {
					splatmapData[i, j, v] = textureValues[v];
				}
			}
		}
	}

	private static List<HexCell> GetAllBeaches() {
		List<HexCell> beaches = new List<HexCell>();
		int beachCount = 0;
		foreach (HexCell riverCell in HexaTerrainData.Instance.River) {
			foreach (HexCell neighboor in riverCell.Neighboors) {
				if (!HexaTerrainData.Instance.River.Contains(neighboor)) {
					neighboor.cellType = CellType.Beach;
					neighboor.originalType = CellType.Beach;
					beaches.Add(neighboor);
					beachCount++;
				}
			}
		}
//		Debug.Log("beachCount: "+beachCount);
		return beaches;
	}
	
	private static List<HexCell> GetAllMountains() {
		List<HexCell> mountains = new List<HexCell>();
		int mountainCount = 0;
		foreach (HexCell mountainCell in HexaTerrainData.Instance.Mountain) {
			mountains.Add(mountainCell);
			mountainCount++;
			foreach (HexCell neighboor in mountainCell.Neighboors) {
				if (!HexaTerrainData.Instance.Mountain.Contains(neighboor)) {
					neighboor.cellType = CellType.Mountain;
					neighboor.originalType = CellType.Mountain;
					mountains.Add(neighboor);
					mountainCount++;
				}
			}
		}
//		Debug.Log("mountainCount: "+mountainCount);
		return mountains;
	}

	private static List<HexCell> GetAllMountainSides() {
		List<HexCell> mountainSides = new List<HexCell>();
		int mountainSideCount = 0;
		foreach (HexCell mountainCell in HexaTerrainData.Instance.MountainSide) {
			mountainSides.Add(mountainCell);
			mountainSideCount++;
			foreach (HexCell neighboor in mountainCell.Neighboors) {
				if (HexaTerrainData.Instance.Mountain.Contains(neighboor) || HexaTerrainData.Instance.MountainSide.Contains(neighboor)) continue;
				neighboor.cellType = CellType.MountainSide;
				neighboor.originalType = CellType.MountainSide;
				mountainSides.Add(neighboor);
				mountainSideCount++;
			}
		}
		return mountainSides;
	}

	private static void SetBaseTexture()
	{
		for (int i = 0; i < alphamapWidth; i++) {
			for (int j = 0; j < alphamapHeight; j++)
			{
				splatmapData[i, j, 0] = 1f;
				splatmapData[i, j, 1] = 0f;
				splatmapData[i, j, 2] = 0f;
				splatmapData[i, j, 3] = 0f;
			}
		}
	}


}