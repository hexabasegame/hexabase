﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FactoryState{
	READY,
	WAITING_FOR_RESOURCES,
	PICKING_RESOURCE,
	PROCESSING_RESOURCE,
	PRODUCING_RESOURCE,
	FULL,
	PAUSED,
	ERROR
}

public class TranslationFactoryState{
	public static string[] translate = new string[] {
		"READY",
		"Destination stocks are empty !",
		"PICKING_RESOURCE",
		"At work",
		"PRODUCING_RESOURCE",
		"Destination is full !",
		"Paused",
		"ERROR"
	};
}

public class HexaFactory : HexaBuilding {

	public List<Resource> inputResource;
	public List<Resource> outputResource;
	public float processTime;
	public int inStockMax = -1;
	public int outStockMax = -1;

	private int INFINITE = -1;
	private int inputStockMax;
    private int outputStockMax;
	private bool isActive = true;
	private FactoryState currentState;
	float timeSpent = 0f;
	private FactoryState saveState = FactoryState.ERROR;
		

	// alert worker visit
	GameObject alertBillBoard = null;
	private Timer factoryIsFullTimer = new Timer(30);
	private Timer noWorkerTimer = new Timer(60);
	[HideInInspector] public bool noWorkerAlert = false;

	private Dictionary<HexaBuilding, int> inRangeBuildings;
	private Dictionary<HexaWareHouse, int> whs = new Dictionary<HexaWareHouse, int>();
	private List<HexaWareHouse> sortedWhs;
	private Dictionary<HexaFactory, int> prodFactories = new Dictionary<HexaFactory, int>();
	private List<HexaFactory> sortedProdFactories;
	public int scanRange = 20;

	public float TimeSpent {
		get {return timeSpent;}
		set {timeSpent = value;}
	}

	public FactoryState CurrentState {
		get { return currentState; }
		set { currentState = value; }
	}

	public FactoryState SaveState {
		get { return saveState;	}
		set { saveState = value; }
	}

	private DateTime _lastCallFromFactoryTimeStamp;
	private DateTime _lastCallToFactoryTimeStamp;

	// CALL FOR WORKER AND RESOURCES //
	/**
	 * This method is used to handle the "ask for resources part". It will try to ask resources to near factories first
	 * but if it has to, it will fallback on the "ask the warehouse for resource".
     */
	IEnumerator CallForWorkerWhenNeeded() {
		while (true) {
			yield return new WaitForSeconds(3);
			if (prodFactories.Count == 0 && whs.Count == 0) { continue; }
			foreach (Resource resource in inputResource) {
				if (resource.stockAmount * 100f / resource.stockAmountMax > 80f) { continue; }
				if (InCoroutineCallForWorkerFromFactories(resource)) {
					_lastCallToFactoryTimeStamp = DateTime.Now;
					break;
				}
				if ((DateTime.Now - _lastCallToFactoryTimeStamp).TotalSeconds > 20) {
					InCoroutineCallForWorkerFromWareHousesForResource(resource);
					break;
				}
			}
		}
	}

	/*
	 This method is used to handle the "ask the warehouse to empty the output stock of the current building".
	 It will try to ask warehouses for a worker, but only if no other factory has asked it for a worker in the last
	 X seconds.
     */
	IEnumerator CallForWarehouseWorkerWhenNeeded() {
		while (true) {
			yield return new WaitForSeconds(8);
			if (whs.Count == 0) { continue; }
			if ((DateTime.Now - _lastCallFromFactoryTimeStamp).TotalSeconds < 8) {continue;}
			foreach (Resource resource in outputResource) {
				if (resource.stockAmount * 100f / resource.stockAmountMax < 20f) { continue; }
				if (InCoroutineCallForWorkerFromWareHouses(resource)) {
					break;
				}
			}
		}
	}

	/*
	 These three method are part of the workers handling. The exists for clarity sake.
	 */
	bool InCoroutineCallForWorkerFromFactories(Resource resource) {
		foreach (HexaFactory factory in sortedProdFactories) {
			if (HexGrid.GetDistanceBetweenTwoCells(_gridPosition, factory._gridPosition) > scanRange) continue;
			if (factory.CanSendWorkerWithResource(resource, _gridPosition)) {
				return true;
			}
		}
		return false;
	}

	/*
	 These three method are part of the workers handling. The exists for clarity sake.
	 */
	bool InCoroutineCallForWorkerFromWareHouses(Resource resource) {
		foreach (HexaWareHouse wh in sortedWhs) {
			if (wh.CanSendWorkerForResource(resource, _gridPosition)) {
				return true;
			}
		}
		return false;
	}

	/*
	 These three method are part of the workers handling. The exists for clarity sake.
	 */
	bool InCoroutineCallForWorkerFromWareHousesForResource(Resource resource) {
		foreach (HexaWareHouse wh in sortedWhs) {
			if (wh.CanSendWorkerWithResource(resource, _gridPosition)) {
				return true;
			}
		}
		return false;
	}

	// PROPERTIES //
	public bool IsActive {
		get { return isActive; }
		set {
			if (value == true && saveState != FactoryState.ERROR)
				currentState = saveState;
			else if (value == false) {
				saveState = currentState;
				currentState = FactoryState.PAUSED;
			}
			isActive = value;
			if (isSelected)
				UIMaster.instance.panelDescription.RefreshFactoryInfoDisplay();
		}
	}

	public int InputStockMax {
		get { return inputStockMax; }
		set {
			inputStockMax = value;
			foreach (Resource res in inputResource) {
				res.stockAmountMax = InputStockMax;
				if (inputStockMax != INFINITE) {
					int leftover = Mathf.Max (0, res.stockAmount - res.stockAmountMax);
					if (leftover > 0)
						RemoveStockResource (res, leftover);
				}
			}
		}
	}

	public int OutputStockMax {
		get { return outputStockMax; }
		set {
			outputStockMax = value;
			foreach (Resource res in outputResource) {
				res.stockAmountMax = outputStockMax;
				if (inputStockMax != INFINITE) {
					int leftover = Mathf.Max (0, res.stockAmount - res.stockAmountMax);
					if (leftover > 0)
						RemoveStockResource (res, leftover);
				}
			}
		}
	}

	// PUBLIC //
	public override void OnConstruction (HexCell cell)
	{
		base.OnConstruction(cell);
		_type = BuildingType.Factory;
		foreach (Resource resource in inputResource)
			ResourceMaster.instance.RegisterResource(resource);
		foreach (Resource resource in outputResource)
			ResourceMaster.instance.RegisterResource(resource);
		TimeMaster.instance.AddBuilding(this);
	}

	public override void OnDestroy()
	{
		base.OnDestroy ();
		if (alertBillBoard != null)
			Destroy (alertBillBoard);
		foreach (HexaWorker worker in Workers) {
			Destroy(worker);
		}
		TimeMaster.instance.RemoveBuilding(this);
	}

	public bool HasEnoughResourcesToProduceOutput ()
	{
		foreach (Resource resource in inputResource){
			if (resource.stockAmount < resource.amount)
				return false;
		}
		return true;
	}

	public bool HasEnoughInputResourceAmount (Resource resourceTarget, int amount)
	{
		Resource resource = GetResourceFromStocks(inputResource, resourceTarget);
		return (resource.stockAmount >= amount);
	}

	public bool HasEnoughOutputSpace ()
	{
		foreach (Resource resource in outputResource) {
			if (resource.stockAmountMax != INFINITE && resource.stockAmount + resource.amount > resource.stockAmountMax)
				return false;
		}
		return true;
	}

    public int DropInputResource(Resource resource, int amount) {
        foreach (Resource factoryResource in inputResource) {
            if (factoryResource.type.name == resource.type.name) {
                return AddInputResource(factoryResource, amount);
            }
        }
		noWorkerTimer.Reset();
        return amount;
    }

	public Resource TakeOutputResource (Resource resourceRequested, int workerCapacity)
	{
		Resource output = GetResourceFromStocks(outputResource, resourceRequested);
		int overCapacity = RemoveStockResource(output, workerCapacity);
		if (currentState == FactoryState.FULL)
			currentState = InitializeProductionState();
		if (isSelected)
			UIMaster.instance.RefreshFactoryDescritptionResourcesDisplay();
		noWorkerTimer.Reset();
		factoryIsFullTimer.Reset();
		return new Resource(output.type, workerCapacity - overCapacity);
	}

    public Resource TakeMostProducedOutputResource(int workerCapacity) {
        Resource resourceRequested = MostProducedResource();
        return TakeOutputResource(resourceRequested, workerCapacity);
    }

    public Resource MostNeededResource() {
        Resource result = null;
        foreach (Resource resource in inputResource) {
            if (result == null || result.stockAmountMax - result.stockAmount > resource.stockAmountMax - resource.stockAmount) {
                result = new Resource(resource);
                result.amount = resource.stockAmountMax - resource.stockAmount;
            }
        }
        return result;
    }

    public Resource MostProducedResource() {
        Resource result = null;
        foreach (Resource resource in outputResource) {
            if (result == null || result.stockAmount < resource.stockAmount) {
                result = new Resource(resource);
                result.amount = resource.stockAmount;
            }
        }
        return result;
    }

	public bool CanSendWorkerWithResource(Resource resource, HexCell destination) {
		foreach (Resource res in outputResource) {
			if (res.type.name != resource.type.name) { continue; }
			if (res.stockAmount < 1) { break; }
			HexaWorker availableWorker = GetAvailableWorker();
			if (availableWorker != null) {


				// Send worker to 'destination'
				List<HexCell> path = HexaPath.GetPathFromBuildingToBuilding(this._gridPosition, destination);
				if (path == null || path.Count == 0) { break; }
				_lastCallFromFactoryTimeStamp = DateTime.Now;
				availableWorker.Behavior.SetNewPath(path);

				// LoadWorkerWithNeededResource
				availableWorker.Carry = resource;
				availableWorker.Carry.amount = 1;

				availableWorker.Origin = this;
				availableWorker.Destination = (HexaBuilding) destination.Building;

				res.stockAmount -= 1;
				return true;
			}
		}
		return false;
	}

	// PRIVATE //
	void Start() {
		base._type = BuildingType.Factory;
		_lastCallFromFactoryTimeStamp = DateTime.MinValue;
		_lastCallToFactoryTimeStamp = DateTime.MinValue;
		InputStockMax = inStockMax;
		OutputStockMax = outStockMax;
 		currentState = FactoryState.READY;
		StartCoroutine("CallForWorkerWhenNeeded");
		StartCoroutine("CallForWarehouseWorkerWhenNeeded");
	}

	void Update ()
	{
		if (IsActive) {
			currentState = StateMachine(currentState);
			if (currentState == FactoryState.FULL) {
				foreach (Resource resource in outputResource) {
					if (resource.stockAmount < resource.stockAmountMax) {
						currentState = InitializeProductionState();
						break;
					}
				}
			}
		}
	}

	int RemoveStockResource(Resource resource, int amountRequested, bool fake=false)
	{
		int unsatisfied = amountRequested - resource.stockAmount;
		unsatisfied = (unsatisfied <= 0) ? 0 : unsatisfied;
		if (!fake)
			resource.stockAmount = Mathf.Max(resource.stockAmount - amountRequested, 0);
		return unsatisfied;
	}

    int AddInputResource (Resource resource, int amount)
    {
        int leftOver = AddStockResource(resource, amount);
        if (currentState == FactoryState.WAITING_FOR_RESOURCES)
            currentState = InitializeProductionState();
        if (isSelected)
            UIMaster.instance.RefreshFactoryDescritptionResourcesDisplay();
        return leftOver;
    }

	int AddStockResource (Resource resource, int amount, bool fake=false)
	{
		int leftover = 0;
		if (resource.stockAmountMax == INFINITE) {
			if (!fake)
				resource.stockAmount += amount;
		} else {
			int total = resource.stockAmount + amount;
			leftover = Mathf.Max (0, total - resource.stockAmountMax);
			if (!fake)
				resource.stockAmount += amount - leftover;
		}
		return leftover;
	}

	Resource GetResourceFromStocks (List<Resource> stocks, Resource resourceToFind)
	{
		foreach(Resource resource in stocks)
		{
			if (resource.type.name == resourceToFind.type.name)
				return resource;
		}
		throw new UnityException("This factory has no resource named : " + resourceToFind.type.name);
	}

	void DisplayAlert ()
	{
		if (alertBillBoard == null) {
			alertBillBoard = Instantiate (UIMaster.instance.alertBillBoard, transform.position, Quaternion.identity);
			alertBillBoard.transform.GetChild(0).transform.position = new Vector3(
				alertBillBoard.transform.position.x,
				modelHeight * alertBillBoard.transform.localScale.y + 2,
				alertBillBoard.transform.position.z
			);
		}
	}

	void HideAlert ()
	{
		noWorkerAlert = false;
		if (alertBillBoard != null) {
			Destroy (alertBillBoard);
		}
		alertBillBoard = null;
	}

	// REFRESH LIST OF NEAR NEIGHBOORS //
	public void ScanRangeWithDistanceForFactories() {
		inRangeBuildings = new Dictionary<HexaBuilding, int>();
		prodFactories = new Dictionary<HexaFactory, int>();
		foreach (HexaFactory factory in TimeMaster.instance.Factories) { // For EVERY factory on map
			int range = HexGrid.GetDistanceBetweenTwoCells(factory.GridPosition, _gridPosition);
			if (range <= scanRange) { // Is the OTHER Destination in range
				foreach (Resource inRes in inputResource) { // For each needed resource for the CURRENT factory
					foreach (Resource outRes in factory.outputResource) { // For each produced resource for the OTHER factory
						if (inRes.Equal(outRes)) { // Does the other factory produce the right resource ?
							prodFactories[factory] = range; // Add the factory to the list
						}
					}
				}
			}
		}
		BuildSortedByRangeListOfFactories();
	}

	public void ScanRangeWithDistanceForWareHouse() {
		inRangeBuildings = new Dictionary<HexaBuilding, int>();
		whs = new Dictionary<HexaWareHouse, int>();
		foreach (HexaWareHouse wareHouse in TimeMaster.instance.WareHouses) {
			int range = HexGrid.GetDistanceBetweenTwoCells(wareHouse.GridPosition, _gridPosition);
			if (range <= scanRange) {
				whs[wareHouse] = range;
			}
		}
		BuildSortedByRangeListOfWareHouse();
	}

	private void BuildSortedByRangeListOfFactories() {
		sortedProdFactories = new List<HexaFactory>();

		for (int i = 0; i < scanRange; i++) {
			foreach (KeyValuePair<HexaFactory, int> factory in prodFactories) {
				if (factory.Value == i) {
					sortedProdFactories.Add(factory.Key);
				}
			}
		}
	}

	private void BuildSortedByRangeListOfWareHouse() {
		sortedWhs = new List<HexaWareHouse>();
		for (int i = 0; i < scanRange; i++) {
			foreach (KeyValuePair<HexaWareHouse, int> factory in whs) {
				if (factory.Value == i) {
					sortedWhs.Add(factory.Key);
				}
			}
		}
	}

	// STATE MACHINE //

	FactoryState StateMachine (FactoryState state)
	{
		if (isSelected)
			UIMaster.instance.panelDescription.RefreshFactoryInfoDisplay ();

		if (noWorkerTimer.IsTime()) {
			noWorkerAlert = true;
			DisplayAlert ();
		} else if (currentState == FactoryState.FULL && factoryIsFullTimer.IsTime ()){
			DisplayAlert ();
		} else {
			HideAlert ();
		}

		switch (state){
		// waiting for external event to happen
		case FactoryState.WAITING_FOR_RESOURCES:
			return WaitingForResource();
		case FactoryState.FULL:
			return FactoryState.FULL;
		case FactoryState.PAUSED:
			return FactoryState.PAUSED;
		// production steps
		case FactoryState.READY:
			return InitializeProductionState();
		case FactoryState.PICKING_RESOURCE:
			return PickInputResourcesState();
		case FactoryState.PROCESSING_RESOURCE:
			return ProcessingResourceState();
		case FactoryState.PRODUCING_RESOURCE:
			return ProduceOutputResourcesState();
		default:
			return FactoryState.ERROR;
		}
	}

	FactoryState WaitingForResource()
	{
		return FactoryState.WAITING_FOR_RESOURCES;
	}

	FactoryState InitializeProductionState ()
	{
		if (!HasEnoughResourcesToProduceOutput())
			return FactoryState.WAITING_FOR_RESOURCES;
		if (!HasEnoughOutputSpace())
			return FactoryState.FULL;
		return FactoryState.PICKING_RESOURCE;
	}

	FactoryState PickInputResourcesState()
	{
		foreach (Resource resource in inputResource)
			RemoveStockResource(resource, resource.amount);
		timeSpent = 0f;
		if (isSelected)
			UIMaster.instance.RefreshFactoryDescritptionResourcesDisplay();
		return FactoryState.PROCESSING_RESOURCE;
	}

	FactoryState ProcessingResourceState()
	{
		if (timeSpent >= processTime)
			return FactoryState.PRODUCING_RESOURCE;
		timeSpent += Time.deltaTime;
		return FactoryState.PROCESSING_RESOURCE;
	}

	FactoryState ProduceOutputResourcesState ()
	{
		foreach (Resource resource in outputResource)
			AddStockResource(resource, resource.amount);
		if (isSelected)
			UIMaster.instance.RefreshFactoryDescritptionResourcesDisplay();
		return FactoryState.READY;
	}
}
