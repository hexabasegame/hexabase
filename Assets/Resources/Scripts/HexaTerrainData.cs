﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class HexaTerrainData {
	private static HexaTerrainData instance;
	private static Terrain _activeTerrain;
	private static float _terrainWidthSize;
	private static float _terrainHeightSize;
	private static int _terrainResolution;
	private static float[,] heights;
	private static readonly Random rand = new Random();
	private static List<HexCell> river = new List<HexCell>();
	private static List<HexCell> mountain = new List<HexCell>();
	private static List<HexCell> mountainSide = new List<HexCell>();
	private static List<HexCell> mountainFeet = new List<HexCell>();
	private static List<HexCell> roads = new List<HexCell>();
	private static readonly float groundLevel = 1f / 600;
	private static readonly float mountainTopLevel = 10f / 600;
	private static readonly float mountainSideLevel = 5f / 600;
	private static readonly float riverBedLevel = 0;

	public static HexaTerrainData Instance {
		get {
			if (instance == null) {
				instance = new HexaTerrainData();
			}
			if (Terrain.activeTerrain == null) {
				Terrain.CreateTerrainGameObject(new UnityEngine.TerrainData());
			}
			return instance;
		}
	}

	public Terrain ActiveTerrain {
		get { return _activeTerrain; }
	}

	public float TerrainWidthSize {
		get { return _terrainWidthSize; }
	}

	public float TerrainHeightSize {
		get { return _terrainHeightSize; }
	}

	public int TerrainResolution {
		get { return _terrainResolution; }
	}

	public float[,] Heights {
		get { return heights; }
		set { heights = value; }
	}

	public List<HexCell> River {
		get { return river; }
		set { river = value;  }
	}

	public static float GroundLevel {
		get { return groundLevel; }
	}

	public static float RiverBedLevel {
		get { return riverBedLevel; }
	}

	public static float MountainTopLevel {
		get { return mountainTopLevel; }
	}

	public List<HexCell> Mountain {
		get { return mountain; }
		set { mountain = value; }
	}

	public static float MountainSideLevel {
		get { return mountainSideLevel; }
	}

	public List<HexCell> MountainSide {
		get { return mountainSide; }
		set { mountainSide = value; }
	}

	public List<HexCell> MountainFeet {
		get { return mountainFeet; }
		set { mountainFeet = value; }
	}

	public List<HexCell> Roads {
		get { return roads; }
		set { roads = value; }
	}

	private HexaTerrainData() {
		if (Terrain.activeTerrain == null) {
			Terrain.CreateTerrainGameObject(new TerrainData());
		}
		_activeTerrain = Terrain.activeTerrain;
		_activeTerrain.transform.position = new Vector3(
			-HexMetrics.innerRadius,
			-1f,
			-HexMetrics.outerRadius
		);
		_activeTerrain.terrainData.size = new Vector3(
			(HexMetrics.innerRadius * 2 * HexMetrics.width + HexMetrics.innerRadius),
			600,
			(HexMetrics.outerRadius * 2 * HexMetrics.height * 3 / 4 + (HexMetrics.outerRadius * 2 / 4))
		);
		_terrainResolution = _activeTerrain.terrainData.heightmapWidth;
		_terrainWidthSize = _activeTerrain.terrainData.size.x / _terrainResolution;
		_terrainHeightSize = _activeTerrain.terrainData.size.z / _terrainResolution;
		heights = _activeTerrain.terrainData.GetHeights(0, 0, _terrainResolution, _terrainResolution);
//		_activeTerrain.terrainData.treeInstances = new TreeInstance[2000];
		PlaceTreesOnTerrain();
//		_trees = new List<TreeInstance>(_activeTerrain.terrainData.treeInstances);
	}
	
	public void ResetTerrain()
	{
		if (Terrain.activeTerrain == null) {
			Terrain.CreateTerrainGameObject(new TerrainData());
			_activeTerrain = Terrain.activeTerrain;
			_activeTerrain.transform.position = new Vector3(
				-HexMetrics.innerRadius,
				-1f,
				-HexMetrics.outerRadius
			);
			PlaceTreesOnTerrain();
		}
	}
	
	public void PlaceTreesOnTerrain()
	{
		Debug.Log("Placing trees on terrain at startup");
		Random rand = new Random();
		float x = ActiveTerrain.terrainData.size.x;
		float z = ActiveTerrain.terrainData.size.z;
		Terrain.activeTerrain.terrainData.treeInstances = new TreeInstance[2000];
		for (int i = 0; i < 2000; i++) {
			double d = rand.NextDouble();
			TreeInstance newTree = new TreeInstance
			{
				position = new Vector3(
					(float) rand.NextDouble(),
					(float) 0,
					(float) rand.NextDouble()
				),
				prototypeIndex = rand.Next(0, 3),
				heightScale = (float)(rand.NextDouble() * (0.3 - 0.15) + 0.15),
				rotation = (float)rand.NextDouble(),
				lightmapColor = new Color32() {a = 255, b = 255, g = 255, r = 255},
				color = new Color32() {a = 255, b = (byte) rand.Next(190, 255), g = (byte) rand.Next(190, 255), r = (byte) rand.Next(190, 255)}
				
			};
			newTree.widthScale = newTree.heightScale;
			Terrain.activeTerrain.AddTreeInstance(newTree);
		}
		Terrain.activeTerrain.Flush();
	}

	public void RemoveTreeFromCell(HexCell cell)
	{
		List<int> toRemoveTreeIndexes = new List<int>();
		int i = 0;
		Vector3 cellPosToTerrainPos = new Vector3(cell.position.x / ActiveTerrain.terrainData.size.x,
			0,
			cell.position.z / ActiveTerrain.terrainData.size.z);
		foreach (TreeInstance tree in Terrain.activeTerrain.terrainData.treeInstances)
		{
			if (Math.Abs(cellPosToTerrainPos.x - tree.position.x) < 0.01 && Math.Abs(cellPosToTerrainPos.z - tree.position.z) < 0.01)
			{
				toRemoveTreeIndexes.Add(i);
			}
			i++;
		}
		toRemoveTreeIndexes.Reverse();
		var trees = new List<TreeInstance>(Terrain.activeTerrain.terrainData.treeInstances);
		foreach (int index in toRemoveTreeIndexes)
		{
			if (index < Terrain.activeTerrain.terrainData.treeInstances.Length)
				trees.RemoveAt(index);
		}
		Terrain.activeTerrain.terrainData.treeInstances = trees.ToArray();
		Terrain.activeTerrain.Flush();
	} 
}
