﻿
using System.Collections.Generic;
using UnityEngine;

public class HexaTier : MonoBehaviour {
    public int level;
    public List<TierRequirements> buildingCountRequirements = null;
}
